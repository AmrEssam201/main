<?php

namespace App\Constants;

class GradeConstant
{
    //    1. special programs:
    //    A. common
    const MIDTERM = 20;
    const REMAINING = 20;
    const CLASS_WORK = 40;
    const TOTAL = 100;

    //    B. with practical
    const PRACTICAL = 10;
    const WRITTEN_EXAM_WITH_PRACTICAL = 50;

    //    C. without practical
    const WRITTEN_EXAM_WITHOUT_PRACTICAL = 60;

//    2. general
    const GENERAL_WRITTEN_EXAM = 50;
    const GENERAL_CLASS_WORK = 50;
    const GENERAL_REMAINING = 30;
}
