<?php

namespace App\Constants;

class GradeDistributionConstant
{
    const REMAINING = 'REMAINING';
    const MIDTERM = 'MIDTERM';
    const WRITTEN_EXAM = 'FINAL';
}
