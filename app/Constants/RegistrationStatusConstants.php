<?php


namespace App\Constants;


class RegistrationStatusConstants
{
    const REGISTERED  = 'REGISTERED';
    const PASS  = 'PASS';
    const FAILED  = 'FAILED';
    const INCOMPLETE  = 'INCOMPLETE';
    const ABSENCE  = 'ABSENCE';
}
