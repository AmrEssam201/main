<?php

namespace App\Exports;

use Illuminate\Support\Facades\Facade;
use App\Constants\GradeConstant;

class ExportHelper extends Facade
{
    private static function incrementChar($char, $increment_by): string
    {
        return chr(ord($char) + $increment_by);
    }

    private static function updateCell($sheet, $data)
    {
        $char = $data['cell'][0];
        $number = $data['cell'][1];
        $horizontal_merge = $data['merge'][0];
        $vertical_merge = $data['merge'][1];
        if ($horizontal_merge > 0) { // horizontalMerge
            $sheet->mergeCells($char . $number . ':' . self::incrementChar($char, $horizontal_merge) . $number);
        }
        if ($vertical_merge > 0) { // verticalMerge
            $sheet->mergeCells($char . $number . ':' . $char . ($number + $vertical_merge));
        }
        $sheet->setCellValue(($char . $number), $data['value']);
    }

    public static function populateSheet($sheet, $sheets_data, $grades_start_cell, $validation_rules, $total = 0, $status = 0)
    {
        $class_work = $validation_rules['class_work'];
        $written_exam = $validation_rules['written_exam'];
        $practical = $validation_rules['has_practical_col'];

        $combined_data = [
            ['cell' => ['B', 7], 'value' => 'العام الدراسى ' . $sheets_data['year'] . ' - ' . $sheets_data['semester_type'], 'merge' => [0, 0]],
            ['cell' => ['B', 10], 'value' => 'الطلاب المسجلين فى مادة ' . $sheets_data['course_concat'], 'merge' => [0, 0]],
            ['cell' => ['G', 10], 'value' => 'العدد: ' . $sheets_data['rows_count'], 'merge' => [0, 0]],
            ['cell' => ['A', 12], 'value' => 'رقم الطالب', 'merge' => [0, 3]],
            ['cell' => ['B', 12], 'value' => 'رقم الجلوس', 'merge' => [0, 3]],
            ['cell' => ['C', 12], 'value' => 'اسم الطالب', 'merge' => [0, 3]],
            ['cell' => ['D', 12], 'value' => 'البريد الالكترونى', 'merge' => [0, 3]],

            //['cell' => [$grades_start_cell, 12], 'value' => $sheets_data['course_name_en'] . '(' . GradeConstant::TOTAL . ')', 'merge' => [1 + $practical, 0]],
            ['cell' => [$grades_start_cell, 12], 'value' => 'نظرى ' . '(' . $written_exam . ')', 'merge' => [1 + $practical, 0]],
            ['cell' => [$grades_start_cell++, 13], 'value' => 'أعمال السنة(' . $class_work . ')', 'merge' => [0, 3]]
        ];
        if ($practical) {
            $combined_data [] = ['cell' => [$grades_start_cell++, 13], 'value' => "Practical \n(" . GradeConstant::PRACTICAL . ')', 'merge' => [0, 2]];
        }
        $combined_data[] = ['cell' => [$grades_start_cell++, 13], 'value' => "Classwork \n(" . $class_work . ')', 'merge' => [0, 2]];
        if ($total) {
            $combined_data[] = ['cell' => [$grades_start_cell++, 12], 'value' => 'Total', 'merge' => [0, 3]];
        }
        if ($status) {
            $combined_data[] = ['cell' => [$grades_start_cell, 12], 'value' => 'State', 'merge' => [0, 3]];
        }
        if (isset($sheets_data['department_name']) and isset($sheets_data['level_state_name'])) {
            $combined_data [] = ['cell' => ['B', 8], 'value' => 'أسماء طلبة ' . $sheets_data['level_name'] . ' - تخصص ' . $sheets_data['department_name'] . ' - ' . $sheets_data['level_state_name'], 'merge' => [0, 0]];
        }
        foreach ($combined_data as $data) {
            self::updateCell($sheet, $data);
        }
    }

    public static function styles($sheet, $grades_start_cell, $practical, $total = 0, $status = 0)
    {
        $start_header_cell = 'A12';
        $increment_by = 1 + $practical + $total + $status;
        $end_header_char = self::incrementChar($grades_start_cell, $increment_by);
        $end_header_number = '15';
        $range_header_cell = $start_header_cell . ':' . $end_header_char . $end_header_number;
        $sheet->getStyle($range_header_cell)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle($range_header_cell)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle($range_header_cell)->getFont()->setBold(true);
        $sheet->getStyle($range_header_cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('999999');

        $sheet->getStyle($grades_start_cell . '13')->getAlignment()->setWrapText(true);
        $sheet->getStyle($end_header_char . '13')->getAlignment()->setWrapText(true);
        $sheet->getStyle('7')->getFont()->setBold(true);
        $sheet->getStyle('8')->getFont()->setBold(true);
        $sheet->getStyle('10')->getFont()->setBold(true);
        $sheet->getStyle('12')->getFont()->setBold(true);

        $sheet->getStyle($start_header_cell . ':' . $end_header_char . $sheet->getHighestRow())->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],
            ],
        ]);
    }
}
