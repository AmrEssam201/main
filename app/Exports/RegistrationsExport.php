<?php

namespace App\Exports;

use App\Models\OpenedCourses;
use App\Models\Registration;
use App\Models\Students;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class RegistrationsExport implements WithMultipleSheets
{

    protected $opened_course;
    protected $validation_rules;

    public function __construct($opened_course, $validation_rules)
    {
        $this->opened_course = $opened_course;
        $this->validation_rules = $validation_rules;
    }

    public function sheets(): array
    {
        $columns = ['level_id', 'department_id', 'level_state_id'];
        $users = Registration::where('opened_course_id', $this->opened_course)->pluck('student_id');
        $maps = Students::whereIn('user_id', $users)->distinct()->get($columns)->toArray();
        $opened_course = OpenedCourses::where('id', $this->opened_course)->get()->load(['course', 'course.courseCode', 'semester', 'semester.SemesterType', 'courseGradeDistributions'])->first();
        $course_name = $opened_course->course->name['ar'];
        $practical = $opened_course->course->practical;
        $course_name_en = $opened_course->course->name['en'];
        $course_code = $opened_course->course->course_code;
        $course_type = $opened_course->course->courseCode->code['ar'];
        $course_concat = $course_type . $course_code . ' - ' . $course_name;
        if ($opened_course->semester) {
            $year = (int)$opened_course->semester->year;
            $semester_type = $opened_course->semester->SemesterType->name['ar'];
        } else {
            $year = null;
            $semester_type = null;
        }
        $sheets = [];
        if ($maps && count($maps) > 0) {
            foreach ($maps as $map) {
                $sheets[] = new RegistrationsSheet($this->opened_course, $map, $year, $semester_type, $course_concat, $course_name_en, $opened_course, $this->validation_rules);
            }
        } else {
            $sheets = [new RegistrationsSheet($this->opened_course, [], $year, $semester_type, $course_concat, $course_name_en, $opened_course, $this->validation_rules)];
        }
        return $sheets;
    }
}
