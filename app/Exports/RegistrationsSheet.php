<?php

namespace App\Exports;

use App\Constants\RegistrationStatusConstants;
use App\Models\Registration;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Constants\DepartmentsConstant;

class RegistrationsSheet implements
    FromCollection,
    WithStrictNullComparison,
    WithMapping,
    WithEvents,
    WithCustomStartCell
{
    use Exportable;

    protected $opened_course;
    protected $conditions;
    protected $title;
    protected $year;
    protected $semester_type;
    protected $course_concat;
    protected $rows_count;
    protected $level_name;
    protected $department_key;
    protected $department_name;
    protected $level_state_name;
    protected $course_name_en;
    protected $grades;
    protected $openCourse;
    protected $grades_start_cell;
    protected $validation_rules;
    protected $total = 1;  // map total grades
    protected $status = 1;  // map students status

    public function __construct($opened_course, $conditions, $year, $semester_type, $course_concat, $course_name_en,
                                $openCourse, $validation_rules)
    {
        $this->opened_course = $opened_course;
        $this->conditions = $conditions;
        $this->year = $year . '/' . ++$year;
        $this->semester_type = $semester_type;
        $this->course_concat = $course_concat;
        $this->course_name_en = $course_name_en;
        $this->openCourse = $openCourse;
        $this->grades_start_cell = 'E';
        $this->validation_rules = $validation_rules;
    }

    public function collection(): Collection
    {
        $student_relation = 'user.student';
        $rows = Registration::where('opened_course_id', $this->opened_course)->whereHas($student_relation, function ($q) {
            $q->where($this->conditions);
        })->get()->load([
            $student_relation . '.level',
            $student_relation . '.department',
            $student_relation . '.levelState',
            'status'
        ]);
        $this->rows_count = count($rows);
        if (isset($rows->first()->user)) {
            $this->level_name = $rows->first()->user->student->level->name['ar'];
            $this->department_key = $rows->first()->user->student->department->key;
            $this->department_name = $rows->first()->user->student->department->name['ar'];
            $this->level_state_name = ($rows->first()->user->student->levelState) ? ($rows->first()->user->student->levelState->name['ar']) : null;
        } else {
            $this->department_name = null;
        }
        return $rows;
    }

    public function map($row): array
    {
        $total = $row->final_grade + $row->semester_work_grade;
        $defaults = [
            $row->user->student->faculty_id,
            $row->user->student->sitting_number,
            $row->user->arabic_full_name,
            $row->user->email,
            $row->final_grade,
        ];
        if ($this->validation_rules['has_practical_col']) {
            $defaults[] = $row->practical;
            $total += $row->practical;
        }
        $defaults[] = $row->semester_work_grade;
        if ($this->total) {
            $defaults[] = $total;
        }
        if ($this->status && self::filterStatus($row->status->key)) {
            $defaults[] = $row->status->name['ar'];
        }
        return $defaults;
    }

    private function filterStatus($status): bool
    {
        if ($status == RegistrationStatusConstants::ABSENCE or $status == RegistrationStatusConstants::INCOMPLETE)
        {
            return true;
        }
        return false;
    }


    public function startCell(): string
    {
        return 'A16';
    }

    private function getTitle(): string
    {
        if ($this->department_key == DepartmentsConstant::GENERAL) {
            $department = 'عام';
        } else {
            $department = $this->department_name ? current(explode(" ", $this->department_name)) : null;
        }
        $title = implode("-", array($this->level_name, $department, $this->level_state_name));
        return mb_substr($title, 0, 31, 'utf-8');
    }

    private function prepareSheetsData()
    {
        return ['year' => $this->year, 'semester_type' => $this->semester_type, 'level_name' => $this->level_name,
            'department_name' => $this->department_name, 'level_state_name' => $this->level_state_name,
            'course_concat' => $this->course_concat, 'rows_count' => $this->rows_count,
            'course_name_en' => $this->course_name_en];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();
                $sheet->setRightToLeft(true);
                $sheet->setTitle($this->getTitle());
                $sheet->getDefaultColumnDimension()->setWidth(13);
                ExportHelper::populateSheet($sheet, $this->prepareSheetsData(), $this->grades_start_cell, $this->validation_rules, $this->total, $this->status);
                ExportHelper::styles($sheet, $this->grades_start_cell, $this->validation_rules['has_practical_col'], $this->total, $this->status);
            },
        ];
    }
}
