<?php

namespace App\Exports;

use App\Models\Student;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;


class Report6Export implements FromCollection , WithMapping , WithHeadings ,WithColumnWidths , WithStyles , WithEvents , WithDrawings
{
    protected $query = null;

    public function __construct($query) {
        $this->query = $query;
    }

    public function collection()
    {
        return $this->query->get();
    }

    public function headings(): array
    {
        return
        [
            [
                ' ' ,
                ' ' ,
                ' ' ,
                '  القسم: ...........  الشعبة: ......... نوع الدراسة (  منتظمين , منتسبين , من الخارج .... الخ )' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' سجل قيد الطلاب والرسوم' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                ' ' ,
                'نموذج رقم ( 522 )  '. PHP_EOL . PHP_EOL . PHP_EOL . ' طبع بمطبعه كليه الفنون التطبيقية - أورمان - جيزة'
            ],
            [
                ' رقم '. PHP_EOL . ' مسلسل ' ,
                'رقم القيد' ,
                'اسم الطالب ولقبه' ,
                ' النوع '. PHP_EOL . ' ذكر ' . PHP_EOL . ' أنثى ' ,
                ' الجنسيه '. PHP_EOL . ' مصرى ' . PHP_EOL . ' وافد وجنسيته ' ,
                'ديانته' ,
                'تاريخ وجهة الميلاد' ,
                ' رقم البطاقة الشخصيةاو العائلية '. PHP_EOL . ' أو جواز السفر للأجنبى ' ,
                '' ,
                'بيانات الطالب العلميه والرياضية والعقوبات التأديبية' ,
                '',
                '' ,
                '' ,
                '' ,
                ' حالة قيده '. PHP_EOL . ' مستجد/منقول ' . PHP_EOL . ' باق محول ' ,
                ' نتيجة  '. PHP_EOL . ' الكشف الطبى ' ,
                ' مده البقاء  '. PHP_EOL . ' بالفرقة ' ,
                ' داخلى  '. PHP_EOL . ' خارجى ' ,
                'نتيجة امتحان الطالب وتقديره (1)' ,
                '' ,
                '' ,
                '' ,
                'نتيجة الامتحانات '. PHP_EOL . ' التكميلية'. PHP_EOL . '  أو المعادله' ,
                'حالات الفصل  '. PHP_EOL . 'أو التحويل '. PHP_EOL . ' وتاريخ اعتمادها' ,
                'الموقف من التجنيد  '. PHP_EOL . 'مرجل/ معاف مؤقت  '. PHP_EOL . 'معاف نهائى' ,
                'الرسوم المسددة' ,
                '' ,
                '' ,
                'رسوم القيد والمصروفات للطلبة الوافدين (2)' ,
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                'عنوان الإقامه بالتفصيل' ,
                'ملاحظات'
            ],
            [
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                'المؤهل الدراسى الخاصل عليه وتاريخه' ,
                'التخصص' ,
                ' مكافآت '. PHP_EOL . ' التفوق المقررة ' ,
                '',
                ' عقوبات تأديبية '. PHP_EOL . ' وتاريخها ' ,
                '' ,
                '' ,
                '',
                '' ,
                ' فى العام السابق ' ,
                '',
                ' فى العام الحالى ' ,
                '',
                '',
                '',
                '',
                'المبلغ',
                'تاريخ السداد',
                ' رقم القسيمه '. PHP_EOL . '  33 غ.ج' ,
                'رسم القيد' ,
                'مصروفات',
                'تاريخ السداد' ,
                'رقم إيصال  '. PHP_EOL . 'البنك' ,
                'النادى' ,
                'تاريخ السداد' ,
                'رقم إيصال  '. PHP_EOL . 'البنك'

            ],
            [
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                'الرقم والتاريخ' ,
                'السجل المدنى' ,
                '' ,
                '' ,
                'مليم' ,
                'جنيه',
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                'النتيجة' ,
                'التقدير' ,
                'النتيجة' ,
                'التقدير'  ,
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
                '' ,
            ]

        ];
    }

    public function map($query): array
    {
        $data =
        [
            [
                '',
                '',
                $query->name,
                $query->gender == 1 ? "انثى" : "ذكر",
                $query->nationality == 0 ? "مصرى" : $query->foreign_nationality ,
                $query->religion == 1 ? "مسيحى" : "مسلم",
                $query->dob . PHP_EOL . $query->pob ,
                $query->national_id ,
                $query->civil_registry ,
                $this->getQualificationDetails($query->qualification , $query->secondry_school, $query->qualification_year),
                '' ,
                '',
                '' ,
                '',
                $this->getLevelStatus($query->level_status),
                $query->medical_exam == 1 ? " لائق " : " غير لائق ",
                '' ,
                '',
                '' ,
                '',
                '',
                '' ,
                '',
                '' ,
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                $query->Address
            ] ,
            [

            ],
            [

            ]
        ];

        foreach($query->fees as $key => $fee) {
          if($data[$key]) {
           $data[$key][25] = $fee->amount. "";
           $data[$key][26] = $fee->date.  "";
           $data[$key][27] = $fee->voucher_number. "";
          } else {
            $data[$key] = ['', '','', '','', '','', '','', '','', '','', '','', '','', '','', '','', '','',  '' , '' , '' , '', '', '','',  '' , '' , '' , ''];
            $data[$key][25] = $fee->amount. "";
            $data[$key][26] = $fee->date.  "";
            $data[$key][27] = $fee->voucher_number. "";
          }
        }
        return $data;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 8,
            'B' => 8,
            'C' => 22,
            'D' => 8,
            'E' => 14,
            'F' => 8,
            'G' => 18,
            'H' => 15,
            'I' => 15,

            'J' => 35,
            'K' => 8,
            'L' => 6 ,
            'M' => 6,

            'O' => 12,
            'P' => 8,
            'Q' => 10,
            'R' => 8,
            's' => 7 ,
            't' => 7 ,
            'u' => 7 ,
            'v' => 7 ,
            'w' => 14 ,
            'x' => 14 ,
            'y' => 16 ,
            'AB' => 12,
            'aa' => 12,
            'AJ' => 20 ,

        ];
    }

    public function styles(Worksheet $sheet) {

        // set alignments cols
        $sheet->getStyle('a:av')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('a:av')->getAlignment()->setVertical('center');
        $sheet->getStyle('a:av')->getAlignment()->setWrapText(true);


        // set alignments rows
        $sheet->getStyle('2')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('2')->getAlignment()->setVertical('center');

        $sheet->getStyle('O1')->getFont()->setSize(28)->setBold(true);

        // merge columns
        $sheet->mergeCells('A2:A4');
        $sheet->mergeCells('B2:B4');
        $sheet->mergeCells('C2:C4');
        $sheet->mergeCells('D2:D4');
        $sheet->mergeCells('E2:E4');
        $sheet->mergeCells('F2:F4');
        $sheet->mergeCells('G2:G4');
        $sheet->mergeCells('j2:N2');
        $sheet->mergeCells('j3:J4');
        $sheet->mergeCells('K3:K4');
        $sheet->mergeCells('L3:M3');
        $sheet->mergeCells('n3:n4');
        $sheet->mergeCells('o2:o4');
        $sheet->mergeCells('p2:p4');
        $sheet->mergeCells('q2:q4');
        $sheet->mergeCells('r2:r4');
        $sheet->mergeCells('a1:c1');
        $sheet->mergeCells('D1:N1');
        $sheet->mergeCells('O1:AB1');
        $sheet->mergeCells('AC1:AH1');
        $sheet->mergeCells('AI1:AK1');

        $sheet->mergeCells('s2:v2');
        $sheet->mergeCells('s3:t3');
        $sheet->mergeCells('u3:v3');
        $sheet->mergeCells('w2:w4');
        $sheet->mergeCells('x2:x4');
        $sheet->mergeCells('y2:y4');

        $sheet->mergeCells('z2:ab2');
        $sheet->mergeCells('z3:z4');
        $sheet->mergeCells('AA3:AA4');
        $sheet->mergeCells('AB3:AB4');

        $sheet->mergeCells('AC2:AH2');

        $sheet->mergeCells('AC3:AC4');
        $sheet->mergeCells('AD3:AD4');
        $sheet->mergeCells('AE3:AE4');
        $sheet->mergeCells('AF3:AF4');
        $sheet->mergeCells('AG3:AG4');
        $sheet->mergeCells('AH3:AH4');
        $sheet->mergeCells('AI3:AI4');
        $sheet->mergeCells('AJ2:AJ4');
        $sheet->mergeCells('AK2:AK4');

        // merge cell + columns [merge area]
        $sheet->mergeCellsByColumnAndRow(8 ,2 , 9 , 3);

        // set row height
        $sheet->getRowDimension(3)->setRowHeight(40);
        $sheet->getRowDimension(1)->setRowHeight(100);

    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setRightToLeft(true);

                $event->sheet->getStyle('A2:AK2')->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],

                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],

                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);

                $event->sheet->getStyle('A3:AK3')->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],

                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],

                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);

                $event->sheet->getStyle('A4:AK4')->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],

                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],

                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);




            },
        ];
    }

    // helper methods
    public function getQualificationDetails($type , $school , $date) {
        $qualification = "";
        if ($type == 0) $qualification = " علمى علوم";
        if ($type == 1) $qualification = " علمى رياضة";
        if ($type == 2) $qualification = " ثانوية فنيه";
        if ($type == 3) $qualification = " الوافدين ";
        if ($type == 4) $qualification = " ستيم ";
        if ($type == 5) $qualification = " السعودية ";
        if ($type == 6) $qualification = " الكويت";
        if ($type == 7) $qualification = " البحرين";
        if ($type == 8) $qualification = " السودان";
        if ($type == 9) $qualification = " الامارات ";
        if ($type == 10) $qualification = " قطر ";
        if ($type == 11) $qualification = " امريكية ";
        if ($type == 12) $qualification = " IG";
        if ($type == 13) $qualification = " مدارس متفوقين";
        if ($type == 14) $qualification = " عمان";
        if ($type == 15) $qualification = " اخري ";

        $data =  " المؤهل : " . $qualification . PHP_EOL;
        $data .= "المدرسة : " . $school. PHP_EOL;
        $data .= "التاريخ : " . $date. PHP_EOL;
        return $data;
    }

    public function getLevelStatus ($level_status) {
        $status = "";
        if ($level_status == 1) $status = " مستجد";
        if ($level_status == 2) $status = " منقول";
        if ($level_status == 3) $status = " محول";
        return $status;

    }

    public function drawings() {
        $drawing = new Drawing();
        $drawing->setName('FCAIH LOGO');
        $drawing->setDescription('This is faculty of computer and information system logo');
        $drawing->setPath(public_path('/images/logo.jpg'));
        $drawing->setHeight(110);
        $drawing->setCoordinates('A1');
        return $drawing;
    }
}
