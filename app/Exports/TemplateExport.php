<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMapping;
use App\Models\Registration;
use App\Models\OpenedCourses;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Events\AfterSheet;

class TemplateExport implements
    FromCollection,
    WithStrictNullComparison,
    WithMapping,
    WithEvents,
    WithCustomStartCell
{
    use Exportable;


    protected $opened_course;
    protected $conditions;
    protected $title;
    protected $year;
    protected $semester_type;
    protected $course_concat;
    protected $rows_count;
    protected $level_name;
    protected $start_Cell = 'A12';
    protected $course_name_en;
    protected $grades_start_cell = 'D';
    protected $program;
    protected $validation_rules;

    public function __construct($opened_course, $validation_rules)
    {
        $this->opened_course = $opened_course;
        $this->validation_rules = $validation_rules;
    }

    public function collection(): Collection
    {
        $opened_course = OpenedCourses::where('id', $this->opened_course)->get()->load(['course', 'course.courseCode', 'semester', 'semester.SemesterType', 'courseGradeDistributions', 'course.program'])->first();

        $course_name = $opened_course->course->name['ar'];
        $this->program = $opened_course->course->program->key;
        $this->course_name_en = $opened_course->course->name['en'];
        $course_code = $opened_course->course->course_code;
        $course_type = $opened_course->course->courseCode->code['ar'];
        $this->course_concat = $course_type . $course_code . ' - ' . $course_name;

        if ($opened_course->semester) {
            $year = (int)$opened_course->semester->year;
            $this->year = $year . '/' . ++$year;
            $this->semester_type = $opened_course->semester->SemesterType->name['ar'];
        }

        $rows = Registration::where('opened_course_id', $this->opened_course)->get();
        $this->rows_count = count($rows);
        return $rows;
    }


    public function map($row): array
    {
        return [];
    }


    public function startCell(): string
    {
        return $this->start_Cell;
    }

    private function prepareSheetsData()
    {
        return ['year' => $this->year, 'semester_type' => $this->semester_type, 'level_name' => $this->level_name,
            'course_concat' => $this->course_concat, 'rows_count' => $this->rows_count,
            'course_name_en' => $this->course_name_en];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();
                $sheet->setRightToLeft(true);
                $sheet->getDefaultColumnDimension()->setWidth(13);
                ExportHelper::populateSheet($sheet, $this->prepareSheetsData(), $this->grades_start_cell, $this->validation_rules);
                ExportHelper::styles($sheet, $this->grades_start_cell, $this->validation_rules['has_practical_col']);
            },
        ];
    }
}
