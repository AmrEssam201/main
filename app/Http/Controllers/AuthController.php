<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function index () {
        return view('login');
    }

    public function home () {
        return view('pages.home');
    }

    public function login (Request $request) 
    {
        try{
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) 
            {
                // Authentication passed...
                return redirect()->intended('home');
            }
            return redirect()->back()->withErrors(['error' => 'الإيميل أو الباسود غلط']);
        }
        catch(Exception $ex)
        {
            return redirect()->back()->withErrors(['error' => 'الداتابيز غير مفتوحة']);
        }
        
        
    }

    public function logout () {
        Auth::logout();
        return view('login');
    }

}
