<?php

namespace App\Http\Controllers;

use App\Constants\ControlConstants;
use App\Models\ControlCourse;
use App\Models\OpenedCourses;
use Illuminate\Http\Request;

class ControlCoursesController extends Controller
{
    protected $opened_courses;
    protected $control_courses;
    public function index()
    {
        $this->opened_courses=OpenedCourses::get();
        $this->control_courses=ControlCourse::get();
        //dd(count($this->control_courses->where('opened_course_id',23)));
        return  view('pages.controlCourses.index', ['courses' => $this->opened_courses,'control_courses'=>$this->control_courses]);

    }
    public function save(Request $request)
    {
        $count = count($request->opened_courses_ids);
        ControlCourse::truncate();
        for ($i = 0; $i < $count; $i++) 
        {
            $opened_course_id = (int)$request->opened_courses_ids[$i];
            if(isset($request->chk_selected[$i]))
            {
                ControlCourse::create([
                    'control_id' =>ControlConstants::CONTROLID,
                    'opened_course_id'=>$opened_course_id
                ]);
            }
        }
        //session(['save_message' => 'تم الحفظ']);
        ///////////////////////////////////////////////////////////////////////////////////
        $this->opened_courses=OpenedCourses::get();
        $this->control_courses=ControlCourse::get();
        return  view('pages.controlCourses.index', ['courses' => $this->opened_courses,'control_courses'=>$this->control_courses]);
    }
}
