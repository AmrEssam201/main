<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DatabaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import_database()
    {
        return view('pages.database.import');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export_database()
    {
        $courses = [
            (object)[
                "id" => 1,
                "code" => "HU-222",
                "name" => " Human Computer Interaction",
                "instructor" => "Manal Abdel Kader",
            ]
        ];
        return view('pages.database.export', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function import_database_post(Request $request)
    {
        $request->validate([
            'db_file' =>
                function ($attribute, $value, $onFailure) {
                    if ($value->getClientOriginalExtension() !== 'sql') {
                        $onFailure('The db file must be a file of type: sql');
                    }
                }
        ]);
        try {
            DB::unprepared(file_get_contents($request->db_file));
            return redirect()->back()->with(["message" => "تم تحديث قاعدة البيانات بنجاح "]);
        }
        catch (\Exception $ex) {
            dd($ex->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function export_database_post(Request $request)
    {
        //
    }
}
