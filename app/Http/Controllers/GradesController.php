<?php

namespace App\Http\Controllers;

use App\Constants\DepartmentsConstant;
use App\Constants\GradeConstant;
use App\Models\Departments;
use App\Models\Levels;
use App\Models\LevelState;
use App\Models\OpenedCourses;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Registration;
use App\Imports\StudentGradesImport;
use App\Models\Students;
use App\Exports\TemplateExport;
use App\Exports\RegistrationsExport;
use App\Constants\RegistrationStatusConstants;
use App\Models\ControlCourse;
use App\Models\Courses;
use App\Models\RegistrationStatus;
use Exception;
use function App\Helpers\getFilePath;
use stdClass;


class GradesController extends Controller
{
    protected $courses;
    protected $students;
    protected $students_count;
    protected $level_id;
    protected $department_id;
    protected $level_state_id;
    protected $level_keys;
    protected $page_size = 20;


    public function __construct()
    {
    }

    public function index()
    {
        $control_courses = ControlCourse::pluck('opened_course_id');
        $courses = OpenedCourses::whereIn("id", $control_courses)->get();
        //dd(count($courses->first()->registrations));
        return view('pages.grades.index', ['courses' => $courses]);
    }

    public function template_export(Request $request)
    {
        $request->merge(['opened_course' => $request->route('opened_course')]);
        $request->validate([
            'opened_course' => 'required|integer|exists:opened_courses,id,deleted_at,NULL',
        ]);
        $opened_course = $request['opened_course'];

        try {
            $file = $this::prepare_export_file_name($opened_course);
            $path = getFilePath("templates", $file);
            $validation_rules = self::prepare_validation_rules($opened_course);
            Excel::store(new TemplateExport($opened_course, $validation_rules), 'public/templates/' . $file);
            return response()->download($path);
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors(['message' => $ex->getMessage()]);
        }
    }
    private static function prepare_export_file_name($opened_course)
    {
        $opened_course_name = OpenedCourses::where('id', $opened_course)->with(['course'])->firstOrFail()->course->name['ar'];
        $opened_course_name = str_replace(' ', '_', $opened_course_name);
        return $opened_course_name . config('config.registration.default_excel_extension');
    }

    public function export_grades(Request $request)
    {

        $request->merge(['opened_course' => $request->route('opened_course')]);
        $request->validate([
            'opened_course' => 'required|integer|exists:opened_courses,id,deleted_at,NULL',
        ]);
        $opened_course = $request['opened_course'];
        $validation_rules = self::prepare_validation_rules($opened_course);
        try {
            $file = $this::prepare_export_file_name($opened_course);
            Excel::store(new RegistrationsExport($opened_course, $validation_rules), 'public/registration/' . $file);
            $path = getFilePath("registration", $file);
            return response()->download($path);
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors(['message' => $ex->getMessage()]);
        }
    }


    public function import_grades(Request $request)
    {
        $request->merge([
            'opened_course' => $request->route('opened_course'),
            'import_file' => $request->file('import_file')
        ]);

        $request->validate([
            'opened_course' => 'required|integer|exists:opened_courses,id,deleted_at,NULL',
            'import_file' => 'required|max:50000|file|mimes:xlsx,xls',
        ]);
        $opened_course = $request['opened_course'];
        $status_id = RegistrationStatus::where('key', RegistrationStatusConstants::REGISTERED)->firstOrFail()->id;
        $users = Registration::where([
            'opened_course_id' => $opened_course,
            'status_id' => $status_id, 'deleted_at' => null
        ])->with('user.student')->get()->pluck('user.student.faculty_id');
        $validation_rules = self::prepare_validation_rules($opened_course);
        try {
            $import = new StudentGradesImport($request, $users, $validation_rules);
            Excel::import($import, $request['import_file']);
            return redirect()->back()->with('message', 'تم إضافة عدد  ' . $import->getRowCount() . ' طالب');
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            $return_data = [];
            foreach ($failures as $failure) {
                $err = " رقم  " . " " . $failure->row() . " : " . $failure->attribute() . " ";
                if (str_contains($failure->errors()[0], 'required')) {
                    if (str_contains($failure->attribute(), 'رقم')) {
                        $err = $err . "غير موجود";
                    } else {
                        $err = $err . "غير موجودة";
                    }
                } else if (str_contains($failure->errors()[0], 'invalid')) {
                    if (str_contains($failure->attribute(), 'رقم')) {
                        $err = $err . "غير صحيح";
                    } else {
                        $err = $err . "غير صحيحة";
                    }
                } else if (str_contains($failure->errors()[0], 'must be')) {

                    $err = $err . "خارج الحدود المسموحة";
                }
                array_push($return_data, $err);
            }
            return redirect()->back()->withErrors($return_data);
        }
    }

    private function prepare_validation_rules($opened_course): array
    {
        $course = OpenedCourses::where('id', $opened_course)->with(['course.program'])->first();
        $program = $course->course->program->key;
        $practical = $course->course->practical;

        $has_practical_col = 0;
        if ($program == DepartmentsConstant::GENERAL) 
        {
            $class_work = GradeConstant::GENERAL_CLASS_WORK;
            $written_exam = GradeConstant::GENERAL_WRITTEN_EXAM;
        } 
        else if ($practical) 
        {
            $class_work = GradeConstant::CLASS_WORK;
            $written_exam = GradeConstant::WRITTEN_EXAM_WITH_PRACTICAL;
            $has_practical_col = 1;
        } 
        else 
        {
            $class_work = GradeConstant::CLASS_WORK;
            $written_exam = GradeConstant::WRITTEN_EXAM_WITHOUT_PRACTICAL;
        }
        return ['written_exam' => $written_exam, 'class_work' => $class_work, 'has_practical_col' => $has_practical_col];
    }

    public function student_grade(Request $request)
    {
        if ($request->course_id == NULL) {
            return redirect()->route('grade_index');
        }
        //////////////////////////////////////////////////////////////

        $validation_rules = self::prepare_validation_rules($request->course_id);
        //dd($validation_rules);
        /////////////////////////////////////////////////////////////
        $courseid = $request->course_id;
        $selectedCourse = OpenedCourses::find($courseid);

        if ($request->level_type == NULL || $request->level_type == "0_0_0") {
            $this->students = Registration::where(["opened_course_id" => $courseid, 'deleted_at' => null])->paginate($this->page_size);
        } else {
            try {
                $this->level_keys = explode('_', $request->level_type);
                $this->students = Registration::where(["opened_course_id" => $courseid, 'deleted_at' => null])->whereHas('user', function ($q) {
                    $q->whereHas('student', function ($nq) {
                        $nq->where("level_id", "=", $this->level_keys[0])
                            ->where("department_id", "=", $this->level_keys[1])
                            ->where("level_state_id", "=", $this->level_keys[2]);
                    });
                })->paginate($this->page_size);
            } catch (Exception $ex) {
                return redirect()->route('grade_index');
            }
        }
        $this->students_count = $this->students->total();

        ///////////////////////////////////////////////////////////////////////////////////
        $columns = ['level_id', 'department_id', 'level_state_id'];
        $users = Registration::where(['opened_course_id' => $courseid, 'deleted_at' => null])->pluck('student_id');
        $maps = Students::whereIn('user_id', $users)->distinct()->get($columns)->toArray();
        $filters = [];
        foreach ($maps as $index => $map) {
            $this->level_id = $map["level_id"];
            $this->department_id = $map["department_id"];
            $this->level_state_id = $map["level_state_id"];
            $filter = new stdClass;
            $filter->id = $this->level_id . "_" . $this->department_id . "_" . $this->level_state_id;
            $filter->level_id = $map["level_id"];
            $filter->department_id = $map["department_id"];
            $filter->level_state_id = $map["level_state_id"];
            $filter->text = Levels::find($this->level_id)->name["ar"] . " - " . Departments::find($this->department_id)->name["ar"] . " - " . LevelState::find($this->level_state_id)->name["ar"];
            $filters[$index] = $filter;
        }
        ///////////////////////////////////////////////////////////////////////////////////

        return view('pages.grades.student_grades', ['students' => $this->students, 'course' => $selectedCourse, 'filters' => $filters, 'students_count' => $this->students_count,'validation_rules' => $validation_rules]);
    }

    public function save(Request $request)
    {
        if ($request->course_id == NULL) {
            return redirect()->route('grade_index');
        }
        $courseid = $request->course_id;

        $this->validate($request, [
            'semester_work_grade[*]' => 'required|min:0|max:50',
            'final_grade[*]' => 'required|min:0|max:50',
        ], [
            'semester_work_grade[*]' => 'درجة أعمال السنة لابد أن تكون من 0 إالى 50',
            'final_grade[*]' => 'درجة النظرى لابد أن تكون من 0 إالى 50'
        ]);

        $count = count($request->s_id);

        for ($i = 0; $i < $count; $i++) {
            $id = (int)$request->s_id[$i];
            $student = Registration::where("student_id", $id)->where("opened_course_id", $courseid)->first();
            $student->semester_work_grade = (int)$request->semester_work_grade[$i];
            $student->final_grade = (int)$request->final_grade[$i];
            if(isset($request->practical_grade[$i]))
            {

                $student->practical = (int)$request->practical_grade[$i];
            }
            $student->status_id = (int)$request->student_status[$i];
            $student->save();
        }
        //session(['save_message' => 'تم الحفظ']);
        ///////////////////////////////////////////////////////////////////////////////////

        return back()->with("save_message", 'تم الحفظ');
    }
    public function savefinalGrade(Request $request)
    {
        $student = Registration::find((int)$request->registration_id);
        $student->final_grade = $request->final_grade;
        $student->save();
        return 1;
    }
    public function saveSemesterWorkGrade(Request $request)
    {
        $student = Registration::find((int)$request->registration_id);
        $student->semester_work_grade = $request->semester_work_grade;
        $student->save();
        return 1;
    }

    public function saveStatus(Request $request)
    {
        $student = Registration::find((int)$request->registration_id);
        $student->status_id = $request->statusId;
        $student->save();
        return 1;
    }
    public function savePractical(Request $request)
    {
        $student = Registration::find((int)$request->registration_id);
        $student->practical = $request->practicalId;
        $student->save();
        return 1;
    }
    
}
