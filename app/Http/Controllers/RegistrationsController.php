<?php

namespace App\Http\Controllers;

use App\Constants\RegistrationStatusConstants;
use App\Models\ControlCourse;
use App\Models\OpenedCourses;
use App\Models\Registration;
use App\Models\RegistrationStatus;
use App\Models\Students;
use Illuminate\Http\Request;
use SebastianBergmann\Environment\Console;

class RegistrationsController extends Controller
{
    protected $student_name;
    protected $student_id;
    protected $result_students;
    protected $user_id;
    protected $student_registered_courses;
    protected $control_courses;
    protected $control_courses_ids;
    protected $current_course;
    protected $default_group_id;

    public function index(Request $request)
    {

        if ($request->student_id != NULL) {
            $this->student_id = $request->student_id;
            $this->result_students = Students::where('faculty_id', $this->student_id)->get();
            if (count($this->result_students) == 0) {
                return redirect()->route('registrations')->withErrors(['notfound' => "لا يوجد طالب برقم " . $this->student_id . " برجاء التأكد من رقم الطالب", 'students' => NULL])->withInput();
            } else {
                return view('pages.registrations.index', ['students' => $this->result_students]);
            }
        } else {
            if ($request->student_name != NULL) {
                $this->student_name = $request->student_name;
                $this->result_students = Students::whereHas('user', function ($q) {
                    $q->where("arabic_full_name", "like", "%" . $this->student_name . "%");
                })->get();
                if (count($this->result_students) == 0) {
                    return redirect()->route('registrations')->withErrors(['notfound' => "لا يوجد طالب باسم " . $this->student_name . " برجاء التأكد من اسم الطالب", 'students' => NULL])->withInput();
                } else {
                    return view('pages.registrations.index', ['students' => $this->result_students]);
                }
            } else {
                return  view('pages.registrations.index', ['students' => null]);
            }
        }
    }
    public function student_registrations(Request $request)
    {
        if ($request->student_id == NULL) 
        {
            return  view('pages.registrations.index', ['students' => null]);
        } 
        else 
        {
            $this->student_id = $request->student_id;
            $this->result_students = Students::where('faculty_id', $this->student_id)->get();
            if (count($this->result_students) == 0) 
            {
                return  view('pages.registrations.index', ['students' => null]);
            } 
            else 
            {
                $this->user_id = $this->result_students->first()->user_id;
                $this->control_courses_ids = ControlCourse::pluck('opened_course_id');
                $this->control_courses = OpenedCourses::whereIn("id", $this->control_courses_ids)->get();
                $this->student_registered_courses = Registration::where(["student_id" => $this->user_id, 'deleted_at' => null])->whereIn("opened_course_id", $this->control_courses_ids)->get();

                if (count($this->control_courses) == 0) 
                {
                    return view('pages.registrations.student_registrations', ['control_courses' => NULL, 'student' => $this->result_students->first()]);
                } 
                else 
                {
                    return view('pages.registrations.student_registrations', ['control_courses' => $this->control_courses, 'student_registered_courses' => $this->student_registered_courses, 'student' => $this->result_students->first()]);
                }
            }
        }
    }

    public function save(Request $request)
    {
        
        $student_id=$request->student_id;
        $this->result_students = Students::where('user_id', $student_id)->get();
        $count = count($request->opened_courses_ids);
        for ($i = 0; $i < $count; $i++) 
        {
            
            $opened_course_id = (int)$request->opened_courses_ids[$i];
            $status_id = RegistrationStatus::where('key', RegistrationStatusConstants::REGISTERED)->firstOrFail()->id;
            if(!isset($request->chk_selected[$i]))
            {
                
               $this->current_course=Registration::where(['student_id'=>$student_id,'opened_course_id'=>$opened_course_id])->get();
               if(count($this->current_course)!=0)
               {
                 $this->current_course->first()->delete();
               }
            }
            else
            {
                $this->current_course=Registration::where(['student_id'=>$student_id,'opened_course_id'=>$opened_course_id ])->onlyTrashed()->get();
                $this->default_group_id=OpenedCourses::find($opened_course_id)->groups()->first()->id;
    
                if(count($this->current_course)!=0)
                {
                    $this->current_course->first()->restore();
                }
                else
                {
                    $this->current_course=Registration::where(['student_id'=>$student_id,'opened_course_id'=>$opened_course_id ])->get();
                    if(count($this->current_course)==0)
                    {
                        Registration::create([
                            'student_id'=>$student_id,
                            'opened_course_id'=>$opened_course_id,
                            'status_id'=> $status_id,
                            'group_id' => $this->default_group_id,
                        ]);
                    }
                }
            }
           
        }
        ///////////////////////////////////////////////////////////////////////////////////
        $this->control_courses_ids = ControlCourse::pluck('opened_course_id');
        $this->control_courses = OpenedCourses::whereIn("id", $this->control_courses_ids)->get();
        $this->student_registered_courses = Registration::where(["student_id" => $student_id, 'deleted_at' => null])->whereIn("opened_course_id", $this->control_courses_ids)->get();
        
        if (count($this->control_courses) == 0) 
        {
            return view('pages.registrations.student_registrations', ['control_courses' => NULL, 'student' => $this->result_students->first()]);
        } 
        else 
        {
            return view('pages.registrations.student_registrations', ['control_courses' => $this->control_courses, 'student_registered_courses' => $this->student_registered_courses, 'student' => $this->result_students->first()]);
        }
    }
}
