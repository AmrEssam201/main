<?php

namespace App\Http\Controllers;

use App\Http\Requests\studentRequest;
use App\Models\Student;
use App\Models\student_fees;
use App\Models\student_grade;
use Illuminate\Http\Request;
use Excel;
use App\Imports\StudentCoursesImport;
use Carbon\Carbon;

class StudentController extends Controller
{
    // public function home () {
    //     return view('pages.home');
    // }

    // public function index (Request $request) {
    //     $conditions = $this->getFilters($request);
    //     $query = new Student();
    //     $query = $conditions != [] ? $query->where($conditions) : $query;
    //     $query = $query->paginate(10);
    //     return view('pages.students.list' , ['students'=> $query]);
    // }

    // public function create () {
    //     return view('pages.students.add');
    // }

    // public function store (studentRequest $request) {
    //     try {
    //         $attributes = $request->all();
    //         if($request->img) {
    //             // SAVE Image First
    //             $imageName = $this->uploadImage($request->img , $request , false);
    //             $attributes['img'] =  $imageName;
    //         }
    //         // SAVE STUDENT DATA
    //         Student::Create($attributes);
    //         return redirect()->back()->with(["message" => "تم اضافة الطالب بنجاح"]);
    //     } catch (\Exception $e) {
    //         return redirect()->back()->withErrors(["error" => "حدث خطأ برجاء المحاولة مرة اخرى"]);
    //     }
    // }

    // public function edit (studentRequest $request) {
    //     $student = Student::findOrFail($request->student);
    //     return view('pages.students.edit' , ['student' => $student]);
    // }

    // public function update (studentRequest $request) {
    //     try {
    //         $attributes = $request->all();
    //         $student = Student::find($attributes['student']);
    //         // SAVE Image First
    //         if($request->img) {
    //             $imageName = $this->uploadImage($request->img , $request , $student->img);
    //             $attributes['img'] =  $imageName;
    //         }
    //         // UPDATE STUDENT DATA
    //         $student->update($attributes);
    //         return redirect()->back()->with(["message" => "تم تحديث بيانات الطالب بنجاح"]);
    //     } catch (\Exception $e) {
    //         return redirect()->back()->withErrors(["error" => "حدث خطأ برجاء المحاولة مرة اخرى"]);
    //     }
    // }

    // public function grades($id) {
    //     $student = Student::findOrFail($id);
    //     return view('pages.students.grade' , [
    //         'grades' => $student->grades ,
    //         'id' => $id
    //     ]);
    // }

    // public function update_grades(Request $request ,$id) {
    //     $request->validate([
    //         'years' => 'array' ,
    //         'years.*' => 'required|numeric|digits:4' ,
    //         'gpas' => 'array' ,
    //         'gpas.*' => 'required|numeric|min:.1|max:4' ,

    //         'student_Status' => 'array' ,
    //         'student_Status.*' => 'nullable|numeric|between:0,7' ,

    //         'excellence_award' => 'array' ,
    //         'excellence_award.*' => 'nullable|numeric|between:0,3' ,

    //         'excellence_award_recieved' => 'array' ,
    //         'excellence_award_recieved.*' => 'nullable|numeric|between:0,1' ,

    //         'degree' => 'array' ,
    //         'degree.*' => 'nullable|numeric' ,

    //     ]);
    //     $student = Student::findOrFail($id);
    //     $student->grades()->delete();
    //     if($request->years) {
    //         foreach($request->years as $index=>$key) {
    //             student_grade::Create([
    //                 'student_id' => $id ,
    //                 'year' => $request->years[$index] ,
    //                 'gpa' => $request->gpas[$index],

    //                 'student_Status' => $request->student_Status[$index],
    //                 'excellence_award' => $request->excellence_award[$index],
    //                 'excellence_award_recieved' => $request->excellence_award_recieved[$index],
    //                 'degree' => $request->degree[$index],
    //             ]);
    //         }
    //     }
    //     return redirect()->back()->with(["message" => "تم تحديث درجات الطالب بنجاح"]);
    // }

    // public function fees($id) {
    //     $student = Student::findOrFail($id);
    //     return view('pages.students.fees' , [
    //         'fees' => $student->fees ,
    //         'id' => $id
    //     ]);
    // }

    // public function update_fees(Request $request ,$id) {
    //     $request->validate([
    //         'amount' => 'array' ,
    //         'amount.*' => 'required|numeric' ,

    //         'date' => 'array' ,
    //         'date.*' => 'required|date' ,

    //         'voucher_number' => 'array' ,
    //         'voucher_number.*' => 'required|string' ,
    //     ]);
    //     $student = Student::findOrFail($id);
    //     $student->fees()->delete();
    //     if($request->date) {
    //         foreach($request->date as $index=>$key) {
    //             student_fees::Create([
    //                 'student_id' => $id ,
    //                 'amount' => $request->amount[$index] ,
    //                 'date' => $request->date[$index] ,
    //                 'voucher_number' => $request->voucher_number[$index]
    //             ]);
    //         }
    //     }
    //     return redirect()->back()->with(["message" => "تم تحديث المعاملات المالية للطالب بنجاح"]);
    // }

    // public function uploadImage ($img , $request , $delete_old) {
    //     $imageName = time().'.'.$img->extension();
    //     $request->img->move(public_path('images/students'), $imageName);
    //     if ($delete_old != false) {
    //             $file_path = public_path().'/images/students/'.$delete_old;
    //             if(file_exists($file_path)) {  unlink($file_path); }
    //     }
    //     return $imageName;
    // }

    // public function getFilters($request) {
    //     $filter = [];
    //     if($request->f_name && $request->f_name != null) { $filter[] = ['name' , 'like' , '%' .$request->f_name. '%'];}
    //     if($request->f_id && $request->f_id != null) { $filter[] = ['student_id' ,'like' , '%' .$request->f_id. '%'];}
    //     if($request->f_level && $request->f_level != "all") { $filter[] = ['level' , $request->f_level];}
    //     if($request->f_status != null && $request->f_status != "all") { $filter[] = ['status', $request->f_status];}
    //     return $filter;
    // }

    // public function destroy (studentRequest $request) {
    //     $student = Student::find($request->student);
    //     $student->grades()->delete();
    //     $student->delete();
    //     return response()->json("student deleted successfully" , 200);
    // }

    // public function uploadBulk (Request $request) {
    //     set_time_limit(0);
    //     $request->validate([
    //         'sheet' => 'required|max:50000|file|mimes:xlsx', 
    //         'bulk_patch' => "required|string"
    //     ]);
    //     $return_data = [];
    //     $bulk_patch = request("bulk_patch");
    //     $level = request("level");
    //     $collection =  Excel::toArray(new StudentCoursesImport(), request()->file('sheet'));
    //     if (isset($collection) && $collection[0]) {
    //         $count = 0;
    //         foreach ($collection[0] as $row) {
    //             $count++;
    //             try {
    //                 $students = Student::updateOrCreate(
    //                     [
    //                         'student_id' => $row[1],
    //                     ],
    //                     [
    //                         'name' => $row[0], 
    //                         'dob' =>Carbon::createFromFormat("d/m/Y" , "30/5/1999"),
    //                         'qualification' => $this->get_qualification($row[3]),
    //                         'secondry_grade' => $row[4],
    //                         'secondry_school' => $row[5] ,
    //                         'gender' => $this->get_gender($row[6]),
    //                         'nationality' => $this->get_nationality($row[7]),
    //                         'foreign_nationality' => $row[8] ? $row[8] : null ,
    //                         'national_id' => $row[9] ,
    //                         'national_id_date' => $row[10] ,
    //                         'student_number' => $row[11] ,
    //                         'university_email' => $row[12] ,
    //                         'father_name' => $row[13] ,
    //                         'mother_name' => $row[14] ,
    //                         'level' => $level ,
    //                         'father_job' => $row[15] ,
    //                         'mother_job' => $row[16] ,
    //                         'Address' => $row[17] ,
    //                         'governorate' =>  $this->getGovernate($row[18]),
    //                         'military_status' => $this->getMilitaryStatus($row[19]),
    //                         'military_reason' => $row[20],
    //                         'three_numbers' => $row[21],
    //                         'military_number' => $row[22],
    //                         'military_date' => $row[23],
    //                         'military_location' => $row[24],
    //                         'military_education' => $this->getMilitaryEducation($row[25]),
    //                         'qualification_year' => $row[26] ,
    //                         'medical_exam' => $this->getMedicalExam($row[27]),
    //                         'religion'=> $this->getReligion($row[28]) ,
    //                         'pob' => $row[29],
    //                         'pob_gov' =>  $row[30] ,
    //                         'civil_registry' => $row[31] ,
    //                         'bulk_patch' => $bulk_patch,
    //                     ]);
    //                     $return_data[] = $students;
    //             } catch (\Exception $e) {
    //                 $return_data[] = ['error' => $e->getMessage() , "id" => $row[1]];
    //             }
    //         }
    //     }
    //     return response()->json($return_data , 200);
    // }

    // public function uploadStatusBulk (Request $request) {
    //     set_time_limit(0);
    //     $request->validate([
    //         'sheet' => 'required|max:50000|file|mimes:xlsx', 
    //         'level' => "required|string" , 
    //         "majority" => "nullable" ,
    //         "year" => "required|string"
    //     ]);
    //     $return_data = [];
    //     $level = request("level");
    //     $year = request("year");
    //     $majority = request("majority");
    //     $collection =  Excel::toArray(new StudentCoursesImport(), request()->file('sheet'));
    //     if (isset($collection) && $collection[0]) {
    //         $count = 0;
    //         foreach ($collection[0] as $row) {
    //             $count++;
    //             try {
    //                 $id = $row[0];
    //                 $gpa = $row[1];
    //                 $degree = $this->getDegree($row[2]);
    //                 $student_status = $this->getStudentStatus($row[3] , $level);
    //                 $status =  student_grade::updateOrCreate(
    //                     [
    //                         'student_id' => $id ,
    //                         'year' => $year ,
    //                     ]
    //                     ,[
    //                         'student_id' => $id ,
    //                         'year' => $year ,
    //                         'gpa' => $gpa,    
    //                         'student_Status' => $student_status,
    //                         'degree' => $degree,
    //                 ]);
    //                 if(isset($status)) {
    //                     $status->student->level = $this->getLevel($student_status , $level);
    //                     $status->student->majority = request('majority') ?  request('majority') : null;
    //                     $status->student->save();
    //                 }
    //                 $return_data[] = $status;
    //             } catch (\Exception $e) {
    //                 $return_data[] = ['error' => $e->getMessage() , "id" => $row[0]];
    //             }
    //         }
    //     }
    //     return response()->json($return_data , 200);
    // }

    // public function getDegree($degree) {
    //     if ($degree == "ضعيف جدا") return "0";
    //     if ($degree == "ضعيف") return "1";
    //     if ($degree == "مقبول") return "2";
    //     if ($degree == "جيد") return "3"; 
    //     if ($degree == "جيد جدا") return "4"; 
    //     if ($degree == "امتياز") return "5"; 
    // }

    // public function getStudentStatus ($stauts , $level) {
    //     if($level == 1 && $stauts == "باق") { return "1"; } 
    //     if($level == 2 && $stauts == "باق") { return "2"; } 
    //     if($level == 3 && $stauts == "باق") { return "3"; } 
    //     if ($stauts == "منقول إلى المستوى الثاني") return "5"; 
    //     if ($stauts == "منقول إلى المستوى الثالث") return "6"; 
    //     if ($stauts == "منقول إلى المستوى الرابع") return "7"; 
    // }

    // public function getLevel ($student_status , $level) {
    //     if($student_status == 1 || $student_status == 2 || $student_status == 3) {
    //         return $level;
    //     } else {
    //         if ($student_status == "5") return "2"; 
    //         if ($student_status == "6") return "3"; 
    //         if ($student_status == "7") return "4"; 
    //     }
    // }

    // public function getReligion ($region) {
    //     if ($region == "مسلم")  { return "1"; }
    //     else return "0";
    // }

    // public function getMedicalExam($status) {
    //     if ( $status == 'لائق') return "1";
    //     else return "0";
    // }

    // public function getMilitaryEducation ($status) {
    //     if ( $status == 'ادى الخدمة') return "1";
    //     else return "0";
    // }

    // public function getMilitaryStatus ($status) {
    //     if ($status == "اعفاء") return "1";
    //     if ($status == "مؤجل") return "2";
    //     if ($status == "تم اداء الخدمة") return "3";
    //     if ($status == "لم يتم تحديد موقفه") return "4"; 
    //     else return "4";
    // }

    // public function get_nationality ($type) {
    //     if ($type == "مصرى" || $type == "مصري") return "0";
    //     else return "1";
    // }

    // public function get_gender ($type) {
    //     if ($type == "ذكر") return "0";
    //     if ($type == "انثى") return "1";
    // }

    // public function get_qualification ($type) {
    //     if ($type == "علمى علوم") return "0";
    //     if ($type == "علمى رياضة") return "1";
    //     if ($type == "ثانوية فنية") return "2";
    //     if ($type == "الوافدين") return "3";
    //     if ($type == "ستيم") return "4";
    //     if ($type == "السعودية") return "5";
    //     if ($type == "الكويت") return "6";
    //     if ($type == "البحرين") return "7";
    //     if ($type == "السودان") return "8";
    //     if ($type == "الامارات") return "9";
    //     if ($type == "قطر") return "10";
    //     if ($type == "امريكية" || $type == "امريكين") return "11";
    //     if ($type == "IG") return "12";
    //     if ($type == "مدارس متفوقين") return "13";
    //     if ($type == "عمان") return "14";
    //     if ($type == "اخري" || $type == "اخرى") return "15";

    // }

    // function getGovernate($governate) {
    //     $governate = trim($governate);
    //     if (($governate) == "القاهرة" || ($governate) == "القاهره" || $governate == "حلوان") {return "1"; }
    //     if (($governate) == "الجيزة" || ($governate) == "الجيزه"  || $governate == "جيزة") {return "2";}
    //     if ($governate == "الاسكندرية" || $governate == "الاسكندريه") {return "3";}
    //     if ($governate == "الدقهلية" || $governate == "الدقهليه") {return "4";}
    //     if ($governate == "البحر الاحمر" || $governate == "البحر الأحمر") {return "5";}
    //     if ($governate == "البحيره" || $governate == "البحيرة") {return "6";}
    //     if ($governate == "الفيوم") {return "7";}
    //     if ($governate == "الغربية" || $governate == "الغربيه") {return "8";}
    //     if ($governate == "الاسماعيلية" || $governate == "الاسماعيليه") {return "9";}
    //     if ($governate == "المنوفيه" || $governate == "المنوفية") {return "10";}
    //     if ($governate == "المنيا") {return "11";}
    //     if ($governate == "القليوبية" || $governate == "القليوبيه") {return "12";}
    //     if ($governate == "الوادى الجديد") {return "13";}
    //     if ($governate == "سينا") {return "14";}
    //     if ($governate == "اسوان") {return "15";}
    //     if ($governate == "اسيوط" || $governate == "أسيوط") {return "16";}
    //     if ($governate == "بنى سويف"  || $governate == "بني سويف") {return "17";}
    //     if ($governate == "بورسعيد") {return "18";}
    //     if ($governate == "دمياط") {return "19";}
    //     if ($governate == "شرقيه" || $governate == "شرقية") {return "20";}
    //     if ($governate == "جنوب سينا") {return "21";}
    //     if ($governate == "كفر الشيخ") {return "22";}
    //     if ($governate == "مطروح") {return "23";}
    //     if ($governate == "الاقصر") {return "24";}
    //     if ($governate == "قنا") {return "25";}
    //     if ($governate == "شمال سينا") {return "26";}
    //     if ($governate == "سوهاج") { return "27";}
    //     else {
    //         return null;
    //     }
    // }
}
