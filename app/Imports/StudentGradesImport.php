<?php

namespace App\Imports;

use App\Constants\GradeConstant;
use App\Models\Registration;
use App\Models\Students;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class StudentGradesImport implements ToCollection, WithStartRow, WithValidation//, SkipsEmptyRows
{
    use Importable;

    protected $opened_course;
    protected $users;
    private $rows = 0;
    private $first_grade_column = 3;
    private $validation_rules;

    public function __construct($request_data, $users, $validation_rules)
    {
        $this->opened_course = $request_data['opened_course'];
        $this->users = $users;
        $this->validation_rules = $validation_rules;
    }

    public function prepareForValidation($data, $index)
    {
        $data[0] = strval($data[0]);
        return $data;
    }

    public function customValidationAttributes(): array
    {
        $grade_index = $this->first_grade_column;
        $defaults = [
            '*.0' => 'رقم الطالب',
            '*.' . $grade_index++ => 'درجة النظرى',
        ];
        if ($this->validation_rules['has_practical_col']) {
            $defaults['*.' . $grade_index++] = 'درجة العملى';
        }
        $defaults['*.' . $grade_index] = 'درجة أعمال السنة';
        return $defaults;
    }

    public function rules(): array
    {
        $grade_index = $this->first_grade_column;
        $defaults = [
            '*.0' => [
                'required',
                'string',
                Rule::In($this->users),
            ],
            '*.' . $grade_index++ => 'required|numeric|between:0,' . $this->validation_rules['written_exam'],
        ];
        if ($this->validation_rules['has_practical_col']) {
            $defaults['*.' . $grade_index++] = 'required|numeric|between:0,' . GradeConstant::PRACTICAL;
        }
        $defaults['*.' . $grade_index ] = 'required|numeric|between:0,' . $this->validation_rules['class_work'];
        return $defaults;
    }

    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            ++$this->rows;
            $user_id = $this->getStudentId($row[0])->user_id;
            $grade_index = $this->first_grade_column;
            $defaults = [
                'final_grade' => $row[$grade_index++],
            ];
            if ($this->validation_rules['has_practical_col']) {
                $defaults['practical'] = $row[$grade_index++];
            }
            $defaults['semester_work_grade'] = $row[$grade_index];
            Registration::where(['student_id'=> $user_id, 'opened_course_id'=> $this->opened_course])->update($defaults);
        }
    }

    public function startRow(): int
    {
        return 16;
    }

    private function getStudentId($faculty_id)
    {
        return Students::where('faculty_id', $faculty_id)->firstOrFail();
    }

    public function getRowCount(): int
    {
        return $this->rows;
    }
}
