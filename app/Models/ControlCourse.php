<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class ControlCourse extends Model
{
    use HasFactory;
    protected $table="control_courses";
    protected $fillable = [
        'control_id', 
        'opened_course_id'
    ];

    public function openedCourse()
    {
        return $this->belongsTo(OpenedCourses::class, 'opened_course_id', 'id');
    }
}
