<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseCode extends Model
{
    use HasFactory;
    protected $table = 'course_codes';
    protected $fillable = [
        'code', 'key'
    ];

    public $translatable = ['code'];

    protected $casts = [
        'code' => 'json'
    ];
}
