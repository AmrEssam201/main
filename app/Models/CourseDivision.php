<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CourseDivision extends Model
{
    use SoftDeletes;
    protected $table = 'course_divisions';
    protected $fillable = [
        'name', 'key'
    ];

    public $translatable = ['name'];

    protected $casts = [
        'name' => 'json'
    ];

}
