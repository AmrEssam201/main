<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\GradeDistribution;

class CourseGradeDistribution extends Model
{
    use SoftDeletes;
    protected $table = 'course_grade_distributions';
    protected $fillable = [
        'grade_distribution_id', 'grade', 'opened_course_id'
    ];

    public $translatable = ['assessment_name'];

    protected $casts = [
        'assessment_name' => 'json'
    ];

    protected $with = [
        'gradeDistribution'
    ];

    public function gradeDistribution()
    {
        return $this->belongsTo(GradeDistribution::class, 'grade_distribution_id', 'id');
    }

    public function studentCoursesDistributionGrades()
    {
        return $this->hasMany(
            StudentCoursesDistributionGrades::class,
            'distribution_id',
            'id',
        );
    }
}
