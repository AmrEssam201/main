<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CourseCode;
use App\Models\Programs;

class Courses extends Model
{
    use SoftDeletes;
    protected $table = 'courses';
    protected $fillable = [
        'name', 'course_code', 'course_code_id', 'key',
        'program_id', 'credit_hours', 'lecture_hours', 'has_section', 'practical'
    ];

    public $translatable = ['name'];

    protected $casts = [
        'name' => 'json'
    ];

    protected $with = [
        'courseCode'
    ];

    public function program()
    {
        return $this->belongsTo(Programs::class, 'program_id', 'id');
    }

    public function courseCode()
    {
        return $this->belongsTo(CourseCode::class, 'course_code_id', 'id');
    }

    public function prerequisites()
    {
        return $this->belongsToMany(Courses::class, 'courses_prerequisites', 'course_id', 'prerequisite_id')
            ->using(CoursesPrerequisites::class);
    }


    public function requirement()
    {
        return $this->hasOne(CoursesRequirements::class, 'course_id', 'id');
    }
}
