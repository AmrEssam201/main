<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CoursesPrerequisites extends Pivot
{
    protected $table = 'courses_prerequisites';
    protected $fillable = [
        'course_id', 'prerequisite_id'
    ];
}
