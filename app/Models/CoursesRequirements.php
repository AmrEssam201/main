<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CourseDivision;
use App\Models\Departments;

class CoursesRequirements extends Model
{
    use SoftDeletes;
    protected $table = 'courses_requirements';
    protected $fillable = [
        'course_id', 'department_id', 'course_division_id'
    ];

    protected $with = [
        'department', 'courseDivision'
    ];

    public function department()
    {
        return $this->belongsTo(Departments::class, 'department_id', 'id');
    }

    public function courseDivision()
    {
        return $this->belongsTo(CourseDivision::class, 'course_division_id', 'id');
    }
}
