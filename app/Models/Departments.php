<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Base\traits\Translatable;

class Departments extends Model
{
    use SoftDeletes;
    protected $table = 'departments';
    protected $fillable = [
        'name', 'program_id', 'key'
    ];

    public $translatable = ['name'];

    protected $casts = [
        'name' => 'json'
    ];


    public function program(){
        return $this->belongsTo(Programs::class, 'program_id','id');
    }

}
