<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Doctors extends Model
{
    use SoftDeletes;

    protected $table = 'doctors';
    protected $fillable = [
        'degree', 'department_id', 'user_id', 'is_outsourced'
    ];

    public $translatable = [];

    protected $casts = [
        'name' => 'json'
    ];

    public function department()
    {
        return $this->belongsTo(Departments::class, 'department_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
