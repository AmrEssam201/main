<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;

class DoctorsGroups extends Pivot
{
    protected $table = 'doctors_groups';
    protected $fillable = [
        'group_id', 'doctor_id'
    ];

    public function groups()
    {
        return $this->belongsTo(Groups::class, 'group_id', 'id');
    }
}
