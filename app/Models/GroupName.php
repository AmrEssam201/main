<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupName extends Model
{
    use SoftDeletes;
    protected $table = 'group_names';
    protected $fillable = [
        'name', 'key'
    ];

    public $translatable = ['name'];

    protected $casts = [
        'name' => 'json'
    ];
}
