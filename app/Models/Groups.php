<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Base\traits\Translatable;
use App\Models\Day;
use App\Models\GroupName;
use App\Models\Place;
use App\Models\User;
use App\Models\DoctorsGroups;

class Groups extends Model
{
    use SoftDeletes;
    protected $table = 'groups';
    protected $fillable = [
        'name', 'start_time', 'end_time', 'day_id', 'opened_course_id', 'place_id'
    ];

    protected $with = ['groupName'];

    public function doctors()
    {
        return $this->belongsToMany(User::class, 'doctors_groups', 'group_id', 'doctor_id')
            ->using(DoctorsGroups::class);
    }

    public function day()
    {
        return $this->belongsTo(Day::class, 'day_id', 'id');
    }

    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id', 'id');
    }

    public function openedCourse()
    {
        return $this->belongsTo(OpenedCourses::class, 'opened_course_id', 'id');
    }

    public function groupName()
    {
        return $this->hasOne(GroupName::class, 'id', 'name');
    }
}
