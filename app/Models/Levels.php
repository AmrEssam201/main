<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Base\traits\Translatable;

class Levels extends Model
{
    use SoftDeletes;
    protected $table = 'levels';
    protected $fillable = [
        'name', 'key', 'level_number'
    ];

    public $translatable = ['name'];

    protected $casts = [
        'name' => 'json'
    ];
}
