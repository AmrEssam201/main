<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Users\Entities\User;

class OpenedCourses extends Model
{
    protected $table = 'opened_courses';
    protected $fillable = [
        'course_id', 'semester_id', 'department_id'
    ];

    public function course()
    {
        return $this->belongsTo(Courses::class, 'course_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Departments::class, 'department_id', 'id');
    }

    public function semester()
    {
        return $this->belongsTo(Semesters::class, 'semester_id', 'id');
    }

    public function levels()
    {
        return $this->belongsToMany(Levels::class, 'opened_courses_levels', 'opened_course_id', 'level_id')
            ->using(OpenedCoursesLevels::class);
    }

    public function groups()
    {
        return $this->hasMany(Groups::class, 'opened_course_id', 'id');
    }

    public function registrations()
    {
        return $this->hasMany(Registration::class, 'opened_course_id', 'id');
    }

    public function courseGradeDistributions()
    {
        return $this->hasMany(CourseGradeDistribution::class, 'opened_course_id', 'id');
    }

    public function examControls()
    {
        return $this->belongsToMany(ExamControl::class,"control_courses","opened_course_id","control_id");
    }

}
