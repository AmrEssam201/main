<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OpenedCoursesLevels extends Pivot
{
    protected $table = 'opened_courses_levels';
    protected $fillable = ['opened_course_id', 'level_id'];
}
