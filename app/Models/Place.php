<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Place extends Model
{
    use SoftDeletes;
    protected $table = 'places';
    protected $fillable = [
        'name', 'location', 'type', 'is_outside_faculty'
    ];

    public $translatable = ['name', 'location'];

    protected $casts = [
        'name' => 'json',
        'location' => 'json'
    ];
}
