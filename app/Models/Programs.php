<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Programs extends Model
{
    use SoftDeletes;
    protected $table = 'programs';
    protected $fillable = [
        'name', 'key'
    ];

    public $translatable = ['name'];

    protected $casts = [
        'name' => 'json'
    ];
}
