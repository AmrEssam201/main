<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Registration extends Model
{
    use SoftDeletes;

    protected $table = 'registrations';
    protected $fillable = [
        'student_id', 'opened_course_id', 'status_id','group_id', 'semester_work_grade','practical', 'final_grade'
    ];

    public function openedCourse()
    {
        return $this->belongsTo(OpenedCourses::class, 'opened_course_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(RegistrationStatus::class, 'status_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo(Groups::class, 'group_id', 'id');
    }

    // public function user()
    // {
    //     return $this->belongsTo(User::class, 'student_id', 'id');
    // }

    public function grade()
    {
        return $this->hasMany(StudentCoursesDistributionGrades::class, 'student_id', 'student_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'student_id', 'id');
    }
}
