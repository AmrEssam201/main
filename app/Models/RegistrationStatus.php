<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistrationStatus extends Model
{
    use SoftDeletes;
    protected $table = 'registration_statuses';
    protected $fillable = [
        'name', 'key'
    ];

    public $translatable = ['name'];

    protected $casts = [
        'name' => 'json'
    ];
}
