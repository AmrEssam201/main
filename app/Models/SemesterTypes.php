<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SemesterTypes extends Model
{
    use SoftDeletes;
    protected $table = 'semester_types';
    protected $fillable = [
        'name', 'key'
    ];

    public $translatable = ['name'];

    protected $casts = [
        'name' => 'json'
    ];
}
