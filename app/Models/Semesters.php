<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\SemesterTypes;

class Semesters extends Model
{
    use SoftDeletes;
    protected $table = 'semesters';
    protected $fillable = [
        'semester_type_id', 'current_semester',
        'start_date', 'end_date', 'midterm_date',
        'registration_start_date', 'registration_end_date',
        'add_drop_start_date', 'add_drop_end_date' , 'year'
    ];


    protected $with = ['SemesterType'];

    public function SemesterType()
    {
        return $this->belongsTo(SemesterTypes::class, 'semester_type_id', 'id');
    }

}
