<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Users\Entities\User;

class StudentCoursesDistributionGrades extends Model
{
    use SoftDeletes;

    protected $table = 'student_courses_distribution_grades';
    protected $fillable = [
        'student_id', 'grade', 'distribution_id'
    ];

    public function distribution()
    {
        return $this->belongsTo(CourseGradeDistribution::class, 'distribution_id', 'id');
    }

    public function student()
    {
        return $this->belongsTo(User::class, 'student_id', 'id');
    }

}
