<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Base\traits\Translatable;
use App\Models\Departments;
use App\Models\Levels;
use App\Models\LevelState;

class Students extends Model
{
    use SoftDeletes;

    protected $table = 'students';
    protected $fillable = [
        'faculty_id', 'gender', 'national_id',
        'phone', 'home_address',
        'emergency_contact', 'GPA', 'level_id',
        'department_id', 'user_id', 'level_state_id', 'sitting_number'
    ];

    public function department()
    {
        return $this->belongsTo(Departments::class, 'department_id', 'id');
    }

    public function level()
    {
        return $this->belongsTo(Levels::class, 'level_id', 'id');
    }

    public function levelState()
    {
        return $this->belongsTo(LevelState::class, 'level_state_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
