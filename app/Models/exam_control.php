<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamControl extends Model
{
    use HasFactory;
    protected $table="exam_controls";
    protected $fillable = [
        'control_name', 
    ];

    public function courses()
    {
        return $this->belongsToMany(OpenedCourses::class,"control_courses","control_id","opened_course_id");
    }

}
