<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControlCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_courses', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('control_id');
            $table->foreign('control_id')->references('id')->on('exam_controls');
            
            $table->unsignedBigInteger('opened_course_id');
            $table->foreign('opened_course_id')->references('id')->on('opened_courses');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_courses');
    }
}
