<?php

namespace Database\Seeders;

use App\Models\ControlUsers;
use App\Models\ExamControl;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        
        ControlUsers::updateOrCreate([
            'email' => 'admin@gmail.com',
        ], [
            'name' => 'ADMIN',
            'password' => bcrypt('123456'),
        ]);
        ControlUsers::updateOrCreate([
            'email' => 'doctor@gmail.com',
        ], [
            'name' => 'DOCTOR',
            'password' => bcrypt('123456'),
        ]);

        
        
    }
}
