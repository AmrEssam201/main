<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary  sidebar-dark accordion sidebar" id="accordionSidebar"
    style="margin-top: 70px;">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
        <div class="sidebar-brand-text mx-1"> نظام الكنترول</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    @if (auth()->user()->name == 'ADMIN')
        <li class="nav-item active">
            <a class="nav-link" href="database_import">
                <i class="fas fa-database mx-2"></i>
                <span>تحديث قاعدة البيانات</span>
            </a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="control_courses">
                <i class="fas fa-list mx-2"></i>
                <span>تحديد مواد الكنترول</span>
            </a>
        </li>

    @endif


    <!-- Nav Item - Excellent Students -->
    <li class="nav-item active">
        <a class="nav-link" href="grades">
            <i class="fas fa-file-alt mx-2"></i>
            <span>ادخال الدرجات </span>
        </a>
    </li>
    <!-- Nav Item - Governorates -->
    <li class="nav-item active">
        <a class="nav-link" href="registrations">
            <i class="fas fa-download mx-2"></i>
            <span> تسجيل المقررات </span>
        </a>
    </li>
    <!-- Nav Item - Excellent Students -->
    <li class="nav-item active">
        <a class="nav-link" href="reports">
            <i class="far fa-file-excel mx-2"></i>
            <span> التقارير</span>
        </a>
    </li>

    @if (auth()->user()->name == 'ADMIN')
    <li class="nav-item active">
        <a class="nav-link" href="database_export">
            <i class="fas fa-download mx-2"></i>
            <span> استخراج قاعدة البيانات </span>
        </a>
    </li>
    @endif

</ul>
<!-- End of Sidebar -->
