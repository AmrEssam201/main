@extends('layout.master')

@section('body')

    <div class="row d-flex flex-column m-md-3">
        <div class="d-sm-flex align-items-center justify-content-center mt-4 mb-5 bg-dark p-3">
            <h1 class="h3 mb-0 text-center text-white">تحديد مواد الكنترول</h1>
        </div>




        @if (@session()->has('save_message'))
            <div class="row mx-0 d-flex justify-content-center align-items-center">
                <div class="bg rounded bg-success text-white p-2 my-2">
                    {{ session()->get('save_message') }}
                </div>
            </div>
        @endif

        @foreach ($errors->all() as $message)
            <div class="row mx-0 d-flex justify-content-center align-items-center">
                <div class="bg rounded bg-danger text-white p-2 m-2">
                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                    {{ $message }}
                </div>
            </div>
        @endforeach
        @if ($courses == null)
            <div class="row mx-0 d-flex justify-content-center align-items-center">
                <div class="bg rounded bg-success text-white p-2 my-2">
                    لا يوجد مقررات
                </div>
            </div>
        @else
            <form method="post" action="">
                <div class="row mx-0">
                    @csrf
                    <div class="table-responsive">

                        <table class="table table-light  table-striped ">
                            <thead class="text-center">
                                <th> # </th>
                                {{-- <th><input type="checkbox" id="all" name="all" class="form-control" disabled/></th> --}}
                                <th></th>
                                <th> كود المادة </th>
                                <th> اسم المادة </th>
                                <th> المدرس </th>
                            </thead>
                            <tbody>

                                @foreach ($courses as $index => $course)
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                                {{ ++$index }}
                                                <input type="hidden" name="opened_courses_ids[]"
                                                    value="{{ $course->id }}" />
                                            </div>
                                        </td>
                                        <td>
                                            <input type="checkbox" name="chk_selected[{{$index-1}}]" class="form-control" 
                                            @if (count($control_courses->where('opened_course_id',$course->id))!=0)
                                                checked
                                            @endif
                                            
                                            />
                                        </td>
                                        <td> {{ $course->course->courseCode->code['ar'] . ' ' . $course->course->course_code }}
                                        </td>
                                        <td> {{ $course->course->name['ar'] }} </td>
                                        <td> {{ $course->groups()->first()->doctors()->first()->arabic_full_name }} </td>

                                    </tr>
                                @endforeach
                            </tbody>

                        </table>

                        <div class="col-12 row d-flex justify-content-center">
                            {{-- {{ $courses->appends($_GET)->links("pagination::bootstrap-4") }} --}}
                        </div>

                    </div>

                </div>
                <div class="row mx-0 d-flex justify-content-center align-items-center">
                    <button type="submit" class="btn  btn-outline-dark btn-inline-block mt-3">
                        <i class="fas fa-save    "></i>
                        حفظ
                    </button>
                </div>
            </form>
        @endif


    </div>
@endsection
