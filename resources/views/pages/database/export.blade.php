@extends('layout.master')

@section('body')

<div class="row d-flex flex-column m-md-3">

    <div class="d-sm-flex align-items-center justify-content-center mt-4 mb-5">
        <h1 class="h3 mb-0 text-gray-600  text-center"> إستخراج  قاعدة البيانات  </h1>
    </div>

    
    @if ( @session()->has('message') )
        <div class="bg rounded bg-success text-white p-2 my-2">
            {{  session()->get('message') }}
        </div>
    @endif

    @foreach ($errors->all() as $message)
        <div class="bg rounded bg-danger text-white p-2 m-2">
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
            {{  $message }}
        </div>
    @endforeach

    {{-- Filter Data  --}}
    <div class="filter-section">
        <form method="GET" action="" class="row mx-0 px-0 d-flex justify-content-center align-items-center">
            <div class="col-md-3 col-sm-6 col-12 pl-0">
                <select class="form-control"  name="semster_year">
                    <option value="all" {{ request("semster_year") == "all" || request("semster_year") == "null" ? "selected": "" }}> اختر السنة الدراسية</option>
                    <option value="1" {{ request("semster_year") == "1" ? "selected": "" }}> 2021-2022</option>
                    <option value="2" {{ request("semster_year") == "2" ? "selected": "" }}> 2020-2021</option>
                </select>
            </div>
            <div class="col-md-3 col-sm-6 col-12 pl-0">
                <select class="form-control"  name="semster_type">
                    <option value="all" {{ request("semster_type") == "all" || request("semster_type") == "null" ? "selected": "" }}>  اختر الفصل الدراسى</option>
                    <option value="1" {{ request("semster_type") == "1"  ? "selected": "" }}> الفصل الدراسى الاول</option>
                    <option value="2" {{ request("semster_type") == "2"  ? "selected": "" }}> الفصل الدراسى الثانى</option>
                    <option value="0" {{ request("semster_type") == "0"  ? "selected": "" }}> الفصل الدراسى الصيفى</option>
                </select>
            </div>
            <div class="col-md-3 col-sm-6 col-12 pl-0">
                <button type="submit" class="btn btn-block btn-outline-light "> <i class="fas fa-filter"></i> بحث </button>
            </div>
        </form>
    </div>
    {{-- Filter Data  --}}

    <div class="row mx-0">

        <div class="table-responsive">

            <table class="table table-light table-striped table-hover">
                <thead>
                    <th> # </th>
                    <th> كود المادة </th>
                    <th> اسم المادة </th>
                    <th> المدرس </th>
                    <th> تحكم  </th>
                </thead>
                <tbody>

                    @foreach ($courses as $course)
                        <tr>
                            <td> 
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="check_course[]" id="" value="{{ $course->id }}">
                                </div> 
                            </td>
                            <td> {{ $course->code }} </td>
                            <td> {{ $course->name }} </td>
                            <td> {{ $course->instructor }} </td>
                            <td>

                                <div class="form-check">
                                    <input type="radio" class="form-check-input" name="all_students[]" id="" value="1" checked>
                                    <label class="pr-4 mb-0"> كل الطلاب </label>
                                </div>

                                <div class="form-check">
                                    <input type="radio" class="form-check-input " name="all_students[]" id="" value="0" checked>
                                    <label class="pr-4 mb-0"> طلاب محددين</label>
                                </div>

                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>

            <div class="col-12 row d-flex justify-content-center">
                {{-- {{ $courses->appends($_GET)->links("pagination::bootstrap-4") }} --}}
            </div>

        </div>

    </div>

</div>

@endsection