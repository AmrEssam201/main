@extends('layout.master')

@section('body')

<div class="row d-flex flex-column m-md-3">

    <div class="d-sm-flex align-items-center justify-content-center mt-4 mb-5">
        <h1 class="h3 mb-0 text-gray-600  text-center"> تحديث قاعدة البيانات </h1>
    </div>

    
    @if ( @session()->has('message') )
        <div class="bg rounded bg-success text-white p-2 my-2">
            {{  session()->get('message') }}
        </div>
    @endif

    @foreach ($errors->all() as $message)
        <div class="bg rounded bg-danger text-white p-2 m-2">
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
            {{  $message }}
        </div>
    @endforeach

    <div class="row mx-0">

        <form action="{{ route("importDB_Post") }}" method="post" enctype="multipart/form-data">
            @csrf


            <button class="btn btn-primary"  type="button" onclick="upload_file()"> إدخال الملف </button>
            <input type="file" name="db_file" class="hide" id="db_file">

            <button class="btn btn-dark" type="submit">  تحديث قاعدة البيانات </button>

        </form>


    </div>

</div>

@endsection

<script>

function upload_file() {
    $("#db_file").click();
}

</script>
