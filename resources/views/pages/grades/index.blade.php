@extends('layout.master')

@section('body')

    <div class="row d-flex flex-column m-md-3">
        <div class="filter-section">
            <div class="align-items-center justify-content-center mt-2 mb-2">
                <h1 class="h3 mb-0 text-gray-600  text-center"> درجات الطلاب </h1>
            </div>
        </div>


        @if (@session()->has('message'))
            <div class="bg rounded bg-success text-white p-2 my-2">
                {{ session()->get('message') }}
            </div>
        @endif
        
        @foreach ($errors->all() as $message)
            <div class="bg rounded bg-danger text-white p-2 m-2">
                <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                {{ $message }}
            </div>
        @endforeach

        {{-- Filter Data --}}
        {{-- <div class="filter-section">

            <form method="GET" action="" class="row mx-0 px-0 d-flex justify-content-center align-items-center">
                <div class="col-md-3 col-sm-6 col-12 pl-0">
                    <select class="form-control" name="semster_year">
                        <option value="all"
                            {{ request('semster_year') == 'all' || request('semster_year') == 'null' ? 'selected' : '' }}>
                            اختر السنة الدراسية</option>
                        <option value="1" {{ request('semster_year') == '1' ? 'selected' : '' }}> 2021-2022</option>
                        <option value="2" {{ request('semster_year') == '2' ? 'selected' : '' }}> 2020-2021</option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-6 col-12 pl-0">
                    <select class="form-control" name="semster_type">
                        <option value="all"
                            {{ request('semster_type') == 'all' || request('semster_type') == 'null' ? 'selected' : '' }}>
                            اختر الفصل الدراسى</option>
                        <option value="1" {{ request('semster_type') == '1' ? 'selected' : '' }}> الفصل الدراسى الاول
                        </option>
                        <option value="2" {{ request('semster_type') == '2' ? 'selected' : '' }}> الفصل الدراسى الثانى
                        </option>
                        <option value="0" {{ request('semster_type') == '0' ? 'selected' : '' }}> الفصل الدراسى الصيفى
                        </option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-6 col-12 pl-0">
                    <button type="submit" class="btn btn-block btn-outline-light "> <i class="fas fa-filter"></i> بحث
                    </button>
                </div>
            </form>
        </div> --}}
        {{-- Filter Data --}}

        <div class="row mx-0">

            <div class="table-responsive">

                <table class="table table-light  table-striped ">
                    <thead class="text-center">
                        <th> # </th>
                        <th> كود المادة </th>
                        <th> اسم المادة </th>
                        <th> المدرس </th>
                        <th> عدد الطلاب </th>
                        <th> تحكم </th>
                    </thead>
                    <tbody>

                        @foreach ($courses as $index => $course)
                            <!-- Modal -->
                            <div class="modal fade" id="import_instructions_{{ $index }}" tabindex="-1"
                                role="dialog" aria-labelledby="import_instructionsLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="import_instructionsLabel"> إدخال طلاب (
                                                {{ $course->course->name['ar'] }} ) </h5>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <h6> من فضلك اتبع الخطواط الأتيه: </h6>
                                            <ul>
                                                <li> برجاء تحميل نموذج ملف الاكسل من <a
                                                        href="template-export/{{ $course->id }}"> هنا </a> </li>
                                                <li> لا تحاول تغيير ترتيب الاعمدة او الصفوف </li>
                                                <li> قم بملء النموذج بالدرجات </li>
                                                <li> قم بملء النموذج مرة اخرى واضغط على استيراد الدرجات </li>
                                            </ul>
                                            <form action="/import_grades/{{ $course->id }}" method="post"
                                                enctype="multipart/form-data">
                                                @csrf
                                                اختر الملف
                                                <input type="file" name="import_file">

                                                <button type="submit"
                                                    class="btn btn-block btn-outline-dark btn-inline-block mt-3">
                                                    <i class="fas fa-file-pdf"></i>
                                                    إستيراد الدرجات
                                                </button>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">غلق</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <tr>
                                <td>
                                    <div class="form-check">
                                        {{ ++$index }}
                                    </div>
                                </td>
                                <td> {{ $course->course->courseCode->code['ar'] . ' ' . $course->course->course_code }}
                                </td>
                                <td> {{ $course->course->name['ar'] }} </td>
                                <td> {{ $course->groups()->first()->doctors()->first()->arabic_full_name }} </td>
                                <td>
                                    {{ count($course->registrations) }} طالب </td>
                                <td>
                                    <a href="student_grade?course_id={{ $course->id }}">
                                        <i class="fas fa-md fa-edit text-dark" data-toggle="tooltip" data-placement="top"
                                            title="ادخال الدرجات">
                                            ادخال الدرجات
                                        </i>
                                    </a>

                                    <a class="mx-3" data-toggle="modal"
                                        data-target="#import_instructions_{{ $index-1 }}" style="cursor: pointer;">
                                        <i class="fas fa-md  fa-file-import text-dark" data-toggle="tooltip"
                                            data-placement="top" title=" استيراد الدرجات">
                                            استيراد الدرجات
                                        </i>
                                    </a>

                                    <a href="/export_grades/{{ $course->id }}">
                                        <i class="fas fa-md fa-file-export text-dark" data-toggle="tooltip"
                                            data-placement="top" title="  إستخراج الدرجات">
                                            إستخراج الدرجات
                                        </i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>

                <div class="col-12 row d-flex justify-content-center">
                    {{-- {{ $courses->appends($_GET)->links("pagination::bootstrap-4") }} --}}
                </div>

            </div>

        </div>

    </div>



@endsection
