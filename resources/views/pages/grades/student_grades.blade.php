@extends('layout.master')

@section('body')

    <div class="row d-flex flex-column m-md-3">

        <div class="d-sm-flex align-items-center justify-content-center mt-4 mb-5 bg-dark p-3">
            <h1 class="h3 mb-0 text-center text-white"> مادة {{ $course->course->name['ar'] }} </h1>
        </div>

        {{-- Filter Data --}}
        <div class="filter-section">
            <form method="GET" action="" class="row mx-0 px-0 d-flex justify-content-center align-items-center">
                <input type="hidden" name="course_id" value="{{ $course->id }}" />

                <div class="col-md-4 col-sm-4 col-12 pl-0">
                    <select class="form-control" name="level_type">
                        <option value="0_0_0" {{ request('level_type') == '0_0_0' ? 'selected' : '' }}> فلتر بالمستويات
                        </option>
                        @foreach ($filters as $filter)
                            <option value="{{ $filter->id }}"
                                {{ request('level_type') == $filter->id ? 'selected' : '' }}>{{ $filter->text }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2 col-sm-2 col-12 pl-0">
                    <button type="submit" class="btn btn-block btn-outline-light "> <i class="fas fa-filter"></i> بحث
                    </button>
                </div>

            </form>
            <div class="row mt-2 mb-0 bold d-flex text-dark h3 justify-content-center align-items-center">
                عدد ({{ $students_count }}) طالب
            </div>
        </div>
        {{-- Filter Data --}}


        @if (@session()->has('save_message'))
            <div class="row mx-0 d-flex justify-content-center align-items-center">
                <div class="bg rounded bg-success text-white p-2 my-2">
                    {{ session()->get('save_message') }}
                </div>
            </div>
        @endif

        @foreach ($errors->all() as $message)
            <div class="row mx-0 d-flex justify-content-center align-items-center">
                <div class="bg rounded bg-danger text-white p-2 m-2">
                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                    {{ $message }}
                </div>
            </div>
        @endforeach


        <form method="post" action="">
            <div class="row mx-0 d-flex justify-content-center align-items-center">
                <div class="table-responsive">
                    @csrf
                    <table class="table table-light table-striped table-hover">
                        <thead>
                            <th> # </th>
                            <th> رقم الطالب </th>
                            <th> رقم الجلوس </th>
                            <th> اسم الطالب </th>
                            @if ($validation_rules["written_exam"] == 50)
                                <th> (50) نظرى </th>
                            @else
                                <th> (60) نظرى </th>
                            @endif
                            @if ($validation_rules["class_work"] == 50)
                                <th> (50) اعمال السنة </th>
                            @else
                                <th> (40) اعمال السنة </th>
                            @endif
                            @if ($validation_rules["has_practical_col"] == 1)
                                <th> (10) العملى </th>
                            @endif

                            <th> الحالة </th>
                        </thead>
                        <tbody>

                            @foreach ($students as $index => $student)
                                <tr>
                                    <td>
                                        <div class="form-check">
                                            {{ ++$index }}
                                            <input type="hidden" name="s_id[{{ $index - 1 }}]"
                                                value="{{ $student->user->id }}">
                                        </div>
                                    </td>
                                    <td> {{ $student->user->student->faculty_id }} </td>
                                    <td> {{ $student->user->student->sitting_number }} </td>
                                    <td> {{ $student->user->arabic_full_name }} </td>
                                    <td>
                                        <input type="number" required min="0" max="{{ $validation_rules["written_exam"] }}"
                                            class="final_grade[] grade-input" value="{{ $student->final_grade }}"
                                            id="final_grade[{{ $index - 1 }}]"
                                            onblur="saveFinaleGrade({{ $student->id }},{{ $index - 1 }})"
                                            name="final_grade[{{ $index - 1 }}]">
                                    </td>
                                    <td>
                                        <input type="number" required min="0" max="{{ $validation_rules["class_work"] }}"
                                            class="class_grade[] grade-input" value="{{ $student->semester_work_grade }}"
                                            id="semester_work_grade[{{ $index - 1 }}]"
                                            onblur="saveSemesterWorkGrade({{ $student->id }},{{ $index - 1 }})"
                                            name="semester_work_grade[{{ $index - 1 }}]">
                                    </td>
                                    @if ($validation_rules["has_practical_col"] == 1)
                                        <td>
                                            <input type="number" required min="0"
                                                max="10"
                                                class="class_grade[] grade-input"
                                                value="{{ $student->practical }}"
                                                id="practical_grade[{{ $index - 1 }}]"
                                                onblur="savePracticalGrade({{ $student->id }},{{ $index - 1 }})"
                                                name="practical_grade[{{ $index - 1 }}]">
                                        </td>
                                    @endif
                                    <td>
                                        <select class="form-control" id="student_status[{{ $index - 1 }}]"
                                            onchange="saveStatus({{ $student->id }},{{ $index - 1 }})"
                                            name="student_status[{{ $index - 1 }}]">
                                            <option value="1" {{ $student->status_id == '1' ? 'selected' : '' }}> مكتمل
                                            </option>
                                            <option value="5" {{ $student->status_id == '5' ? 'selected' : '' }}> غير
                                                مكتمل
                                            </option>
                                            <option value="6" {{ $student->status_id == '6' ? 'selected' : '' }}> غياب
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>

                    </table>

                </div>
                <div class=" row mt-3 mb-3">
                    {{ $students->appends(request()->input())->links('pagination::bootstrap-4') }}
                </div>


            </div>
            @if (count($students) != 0)
                <div class="row mx-0 d-flex justify-content-center align-items-center">
                    <button type="submit" class="btn  btn-outline-dark btn-inline-block mt-3">
                        <i class="fas fa-save    "></i>
                        حفظ
                    </button>
                </div>
            @endif

        </form>
    </div>

@endsection
<script>
    function saveFinaleGrade(id, finalGradeId) {
        var finalGrade = document.getElementById("final_grade[" + finalGradeId + "]").value;
        if (finalGrade >= 0 && finalGrade <= 50) {
            $.ajax({
                url: "/savefinalGrade",
                type: "get",
                data: {
                    registration_id: id,
                    final_grade: finalGrade
                },
                cache: false,
                success: function() {

                }
            });
        }
    }

    function saveSemesterWorkGrade(id, semesterGradeId) {
        var semesterGrade = document.getElementById("semester_work_grade[" + semesterGradeId + "]").value;
        if (semesterGrade >= 0 && semesterGrade <= 50) {
            $.ajax({
                url: "/saveSemesterWorkGrade",
                type: "get",
                data: {
                    registration_id: id,
                    semester_work_grade: semesterGrade
                },
                cache: false,
                success: function() {

                }
            });
        }
    }

    function saveStatus(id, statusId) {
        var statusValueId = document.getElementById("student_status[" + statusId + "]").value;
        $.ajax({
            url: "/saveStatus",
            type: "get",
            data: {
                registration_id: id,
                statusId: statusValueId
            },
            cache: false,
            success: function() {

            }
        });
    }
    function savePracticalGrade(id, practicalId) {
        var practicalValue = document.getElementById("practical_grade[" + practicalId + "]").value;
        $.ajax({
            url: "/savePractical",
            type: "get",
            data: {
                registration_id: id,
                practicalId: practicalValue
            },
            cache: false,
            success: function() {

            }
        });
    }
</script>
