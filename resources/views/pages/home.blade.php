@extends('layout.master')

@section('body')

    <div class="row d-flex flex-column m-md-3">

        <div class="d-sm-flex align-items-center justify-content-center mt-4 mb-5">
            <h1 class="h3 mb-0 text-gray-600  text-center"> مرحبا بك فى نظام الكنترول </h1>
        </div>

        <div class="row">
            @if (auth()->user()->name == 'ADMIN')
            <!--  UPDATE DATABASE -->
            <a href="/database_import" class="col-md-6 mb-4 home-box-1">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-md font-weight-bold text-danger text-uppercase mb-1">
                                    تحديث قاعدة البيانات</div>
                                <div class="text-xs mb-0 font-weight-bold  text-danger">
                                    <i class="fas fa-exclamation-triangle fa-x text-danger"></i>
                                    غير مسموح الدخول او التعديل الا من قبل المشرفين
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-database  fa-2x text-danger"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
            <!--  SPECIFY CONTROL COURSES -->
            <a href="/control_courses" class="col-md-6 mb-4 home-box-1">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-md font-weight-bold text-danger text-uppercase mb-1">
                                    تحديد مواد الكنترول</div>
                                <div class="text-xs mb-0 font-weight-bold  text-danger">
                                    <i class="fas fa-exclamation-triangle fa-x text-danger"></i>
                                    غير مسموح الدخول او التعديل الا من قبل المشرفين
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-list  fa-2x text-danger"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @endif

            <!--   iNSERT Grades -->
            <a href="/grades" class="col-md-6 mb-4 home-box-2 ">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-md font-weight-bold text-success text-uppercase mb-1">
                                    إدخال الدرجات</div>
                                <p class="text-xs ">
                                    <span class="d-block "> ادخال وإستخراج الدرجات </span>
                                </p>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-file-alt fa-2x text-success"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            <!-- Registration Courses to Students -->
            <a href="/registrations" class="col-md-6 mb-4 home-box-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-md font-weight-bold text-warning text-uppercase mb-1">
                                    تسجيل المقررات</div>
                                <p class="text-xs ">
                                    <span class="d-block "> تسجيل وإلغاء المقررات للطلاب </span>

                                </p>
                            </div>
                            <div class="col-auto">
                                <i class="far fa-file-excel fa-2x text-warning"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @if (auth()->user()->name == 'ADMIN')
            <!-- Export Database -->
            <a href="/database_export" class="col-md-6 mb-4 home-box-3">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-md font-weight-bold text-danger text-uppercase mb-1">
                                    إستخراج قاعدة البيانات</div>
                                <p class="text-xs ">
                                    <span class="d-block "> تصدير قاعدة البيانات </span>
                                    <span class="d-block ">كل الطلاب </span>
                                    <span class="d-block "> طلاب محددين </span>
                                </p>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-download fa-2x text-danger"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @endif
            <!-- Reports -->
            <a href="/reports" class="col-md-6 mb-4 home-box-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-md font-weight-bold text-primary text-uppercase mb-1">
                                    التقارير</div>
                                <p class="text-xs ">
                                    <span class="d-block "> كشف الرصد </span>
                                    <span class="d-block "> كشف الاعلان </span>
                                    <span class="d-block "> درجات اعمال السنه</span>
                                </p>
                            </div>
                            <div class="col-auto">
                                <i class="far fa-file-excel fa-2x text-primary"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>



        </div>

    </div>

@endsection
