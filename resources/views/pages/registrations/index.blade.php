@extends('layout.master')

@section('body')

    <div class="row d-flex flex-column m-md-3">
        <div class="d-sm-flex align-items-center justify-content-center mt-4 mb-5 bg-dark p-3">
            <h1 class="h3 mb-0 text-center text-white"> تسجيل مقررات الطالب </h1>
        </div>

        {{-- Filter Data --}}
        <div class="filter-section">
            <form method="GET" action="" class="row mx-0 px-0 d-flex justify-content-center align-items-center">
                {{-- <input type="hidden" name="course_id" value="{{ $course->id }}" /> --}}

                <div class="col-md-4 col-sm-4 col-12 pl-0">
                    <input type="text" name="student_name" class="form-control" id="student_name"
                        value="{{ old('student_name') }}" placeholder="ادخل اسم الطالب" />

                </div>
                <div class="col-md-4 col-sm-4 col-12 pl-0">
                    <input type="number" id="student_id" value="{{ old('student_id') }}" name="student_id"
                        class="form-control" placeholder="ادخل رقم الطالب" />
                </div>
                <div class="col-md-2 col-sm-2 col-12 pl-0">
                    <button type="submit" class="btn btn-block btn-outline-light "> <i class="fas fa-filter"></i> بحث
                    </button>
                </div>

            </form>
            {{-- <div class="row mt-2 mb-0 bold d-flex text-dark h3 justify-content-center align-items-center">
                عدد ({{ $students_count }}) طالب
            </div> --}}
        </div>
        {{-- Filter Data --}}


        @if (@session()->has('save_message'))
            <div class="row mx-0 d-flex justify-content-center align-items-center">
                <div class="bg rounded bg-success text-white p-2 my-2">
                    {{ session()->get('save_message') }}
                </div>
            </div>
        @endif

        @foreach ($errors->all() as $message)
            <div class="row mx-0 d-flex justify-content-center align-items-center">
                <div class="bg rounded bg-danger text-white p-2 m-2">
                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                    {{ $message }}
                </div>
            </div>
        @endforeach
        @if ($students != NULL)
            <div class="row mx-0">

                <div class="table-responsive">

                    <table class="table table-light  table-striped ">
                        <thead class="text-center">
                            <th> # </th>
                            <th> رقم الطالب </th>
                            <th> رقم الجلوس </th>
                            <th> اسم الطالب </th>
                            <th> تحكم </th>
                        </thead>
                        <tbody>

                            @foreach ($students as $index => $student)
                                <tr>
                                    <td>
                                        <div class="form-check">
                                            {{ ++$index }}
                                        </div>
                                    </td>
                                    <td> {{ $student->faculty_id }} </td>
                                    <td> {{ $student->sitting_number }} </td>
                                    <td> {{ $student->user->arabic_full_name }} </td>
                                    <td>
                                        {{-- <a href="student_grade?course_id={{ $course->id }}"> --}}
                                        <a href="student_registrations?student_id={{ $student->faculty_id }}">
                                            <i class="fas fa-md fa-edit text-dark" data-toggle="tooltip"
                                                data-placement="top" title="المقررات">
                                                المقررات
                                            </i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>

                    <div class="col-12 row d-flex justify-content-center">
                        {{-- {{ $courses->appends($_GET)->links("pagination::bootstrap-4") }} --}}
                    </div>

                </div>

            </div>
        @endif


    </div>
@endsection
