@extends('layout.master')

@section('body')

<div class="row d-flex flex-column m-md-3">

    <div class="d-sm-flex align-items-center justify-content-center mt-4 mb-5">
        <h1 class="h3 mb-0 text-gray-600  text-center"> التقارير </h1>
    </div>

    
    @if ( @session()->has('message') )
        <div class="bg rounded bg-success text-white p-2 my-2">
            {{  session()->get('message') }}
        </div>
    @endif

    @foreach ($errors->all() as $message)
        <div class="bg rounded bg-danger text-white p-2 m-2">
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
            {{  $message }}
        </div>
    @endforeach

    {{-- Filter Data  --}}
    <div class="filter-section">
        <form method="GET" action="" class="row mx-0 px-0 d-flex justify-content-center align-items-center">
            <div class="row col-12 mx-0 justify-content-center align-items-center mb-2">
                <div class="col-md-4  col-sm-6 col-12 pl-0">
                    <select class="form-control"  name="semster_year">
                        <option value="all" disabled selected {{ request("semster_year") == "all" || request("semster_year") == "null" ? "selected": "" }}> اختر السنة الدراسية</option>
                        <option value="1" {{ request("semster_year") == "1" ? "selected": "" }}> 2021-2022</option>
                        <option value="2" {{ request("semster_year") == "2" ? "selected": "" }}> 2020-2021</option>
                    </select>
                </div>
            </div>

            <div class="row col-12 mx-0 justify-content-center align-items-center mb-2">
                <div class="col-md-4 col-sm-6 col-12 pl-0">
                    <select class="form-control" name="semster_type">
                        <option value="all" disabled selected   {{ request("semster_type") == "all" || request("semster_type") == "null" ? "selected": "" }}>  اختر الفصل الدراسى</option>
                        <option value="1" {{ request("semster_type") == "1"  ? "selected": "" }}> الفصل الدراسى الاول</option>
                        <option value="2" {{ request("semster_type") == "2"  ? "selected": "" }}> الفصل الدراسى الثانى</option>
                        <option value="0" {{ request("semster_type") == "0"  ? "selected": "" }}> الفصل الدراسى الصيفى</option>
                    </select>
                </div>
            </div>

            <div class="row col-12 mx-0 justify-content-center align-items-center mb-2">
                <div class="col-md-4 col-sm-6 col-12 pl-0">
                    <select class="form-control" name="semster_type">
                        <option value="all" disabled selected   {{ request("subject") == "all" || request("subject") == "null" ? "selected": "" }}> اسم المادة </option>
                        <option value="1" {{ request("subject") == "1"  ? "selected": "" }}> المادة 1 </option>
                        <option value="2" {{ request("subject") == "2"  ? "selected": "" }}> المادة 2</option>
                    </select>
                </div>
            </div>

            <div class="row col-12 mx-0 justify-content-center align-items-center mb-2">
                <div class="col-md-4 col-sm-6 col-12 pl-0">
                    <select class="form-control" name="report_type">
                        <option value="all" disabled selected   {{ request("report_type") == "all" || request("report_type") == "null" ? "selected": "" }}> نوع التقرير </option>
                        <option value="1" {{ request("report_type") == "1"  ? "selected": "" }}> كشف الرصد </option>
                        <option value="2" {{ request("report_type") == "2"  ? "selected": "" }}> كشف الاعلان </option>
                        <option value="3" {{ request("report_type") == "3"  ? "selected": "" }}> درجات اعمال السنه </option>
                    </select>
                </div>
            </div>

            <div class="row col-12 mx-0 justify-content-center align-items-center mb-2">
                <div class="col-md-4 col-sm-6 col-12 pl-0">
                    <div class="form-check">
                        <input type="radio" class="form-check-input " name="all_students[]" id="" value="1" checked>
                        <label class="pr-4 mb-0 text-white"> كل الطلاب </label>
                    </div>

                    <div class="form-check">
                        <input type="radio" class="form-check-input " name="all_students[]" id="" value="0" >
                        <label class="pr-4 mb-0 text-white"> طلاب محددين</label>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-12 pl-0 mt-4">
                <button type="submit" class="btn btn-block btn-outline-light "> 
                        <i class="fas fa-file-pdf    "></i>
                     طباعة التقرير 
                </button>
            </div>
        </form>
    </div>

</div>

@endsection