@extends('layout.master')

@section('body')

<div class="header my-5">
    <h3 class=" float-right"> كشوف الطلاب</h3>
    <div class="clearfix"></div>
</div>

@if ( @session()->has('message') )
    <div class="bg rounded bg-success text-white p-2 m-2">
        {{  session()->get('message') }}
    </div>
@endif

<div class="filter-section bg-light px-0">
    <form method="get" action="" class="row col-12 mx-0 px-0 d-flex justify-content-start">

        <div class="col-md-4 sol-sm-6 pl-0 col-sm-6 col-12">
            <input type="number" placeholder="سنه الرسوم المسددة"
             class="form-control" name="f_fees_date" value="{{ request("f_fees_date") }}">
        </div>

        {{-- Level  --}}
        <div class="col-md-4 sol-sm-6  col-sm-6 col-12">
            <select class="form-control" placeholder="Select Level" class="form-control" name="f_level">
                <option value="" > جميع المستويات </option>
                <option value="1" {{ request("f_level") == "1" ? "selected" : "" }}>  المستوى الاول</option>
                <option value="2" {{ request("f_level") == "2" ? "selected" : "" }}> المستوى الثاني</option>
                <option value="3" {{ request("f_level") == "3" ? "selected" : "" }}>  المستوى الثالث</option>
                <option value="4" {{ request("f_level") == "4" ? "selected" : "" }}>  المستوى الرابع</option>
            </select>
        </div>

        <div class="col-12 row my-5 mx-0">
            <input type="hidden" name="excel" value="yes">
            <button  type="submit" class="btn btn-success"> طباعة التقرير  <i class="fas fa-file-csv ml-2"></i> </button>
        </div>
    </form>
</div>

@endsection
