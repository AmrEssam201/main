<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ControlCoursesController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\DatabaseController;
use App\Http\Controllers\GradesController;
use App\Http\Controllers\RegistrationsController;
use App\Models\Registration;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ AuthController::class , 'index'])->name("main")->middleware("guest");
Route::post('/login', [ AuthController::class , 'login'])->name("login")->middleware("guest");

// Middle to Admins Only
Route::middleware(['auth'])->group(function () {
    Route::post('/logout', [ AuthController::class , 'logout'])->name("logout");
    Route::get('/home', [ AuthController::class , 'home'])->name("home");

    // Database Controller
    Route::get('database_import', [DatabaseController::class , 'import_database'])->name("importDB");
    Route::post('database_import_post', [DatabaseController::class , 'import_database_post'])->name("importDB_Post");
    Route::get('database_export', [DatabaseController::class , 'export_database'])->name("exportDB");


    // Reports Controller
    Route::get('reports', [ReportController::class , 'index'])->name("report_index");

    // Grades Controller
    Route::get('grades', [GradesController::class , 'index'])->name("grade_index");
    
    Route::get('template-export/{opened_course}', [GradesController::class , 'template_export'])->name("template_export");
    Route::get('export_grades/{opened_course}', [GradesController::class , 'export_grades'])->name("export_grades");
    Route::post('import_grades/{opened_course}', [GradesController::class , 'import_grades'])->name("import_grades");
    Route::get('student_grade', [GradesController::class , 'student_grade'])->name("student_grade");
    Route::post('student_grade', [GradesController::class , 'save'])->name("student_grade");
    Route::get('savefinalGrade', [GradesController::class , 'savefinalGrade'])->name("savefinalGrade");
    Route::get('saveSemesterWorkGrade', [GradesController::class , 'saveSemesterWorkGrade'])->name("saveSemesterWorkGrade");
    Route::get('saveStatus', [GradesController::class , 'saveStatus'])->name("saveStatus");
    Route::get('savePractical', [GradesController::class , 'savePractical'])->name("savePractical");
    
    
    Route::get('registrations', [RegistrationsController::class , 'index'])->name("registrations");
    Route::get('student_registrations', [RegistrationsController::class , 'student_registrations'])->name("student_registrations");
    Route::post('student_registrations', [RegistrationsController::class , 'save'])->name("student_registrations");
    Route::get('control_courses', [ControlCoursesController::class , 'index'])->name("control_courses");
    Route::post('control_courses', [ControlCoursesController::class , 'save'])->name("control_courses");;


    // Route::resource('/students', StudentController::class);
    // Route::get('/students/{id}/manage-grades', [StudentController::class , 'grades']);
    // Route::post('/update_grades/{id}', [StudentController::class , 'update_grades'])->name("update_grade");

    // Route::get('/students/{id}/manage-fees', [StudentController::class , 'fees']);
    // Route::post('/update_fees/{id}', [StudentController::class , 'update_fees'])->name("update_fees");

    // Route::get('/excellent_students', [ReportController::class , 'excellent_students'])->name("excellent");
    // Route::get('/students_numbers', [ReportController::class , 'students_numbers'])->name("students_numbers");
    // Route::get('/governorates', [ReportController::class , 'governorates'])->name("governorates");
    // Route::get('/military_education', [ReportController::class , 'military_education'])->name("military_education");
    // Route::get('/student_ages', [ReportController::class , 'student_ages'])->name("student_ages");
    // Route::get('/military_service', [ReportController::class , 'military_service'])->name("military_service");
    // Route::get('/foreign_students', [ReportController::class , 'foreign_students'])->name("foreign_students");
    // Route::get('/Students_revealed', [ReportController::class , 'Students_revealed'])->name("Students_revealed");

});
